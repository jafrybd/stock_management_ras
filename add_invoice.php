<?php

session_start();


if (isset($_SESSION['store_management_user_id'])) {
  $store_id = $_SESSION['store_management_store_id'];
  $invoiceCreatorId = $_SESSION['store_management_user_id_no'];
  include_once('controller/addInvoiceToCard.php');
  $selectedData = $_SESSION["stockManagement_SelectedProductList"];

  $quary = "SELECT * FROM `stock` INNER join `product`where stock.product_id =  product.id and product.store_id = " . $store_id;
  $resultDataList = mysqli_query($conn, $quary);

  $quary = "SELECT * FROM `category` where sub_cat_id = 0 and store_id = " . $store_id;
  $resultCatList = mysqli_query($conn, $quary);
} else {
  header("location: login.php");
}

?>


<!DOCTYPE html>
<html lang="en">

<head>
  <?php include_once('partials/header_common.php');  ?>

  <!-- iCheck -->
  <link href="public/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
  <!-- Datatables -->
  <link href="public/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
  <link href="public/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
  <link href="public/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
  <link href="public/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
  <link href="public/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

  <!-- bootstrap-daterangepicker -->
  <link href="public/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
  <!-- bootstrap-datetimepicker -->
  <link href="public/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">


</head>

<body class="nav-md">
  <div class="container body">
    <div class="main_container">
      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">

          <!-- menu profile quick info -->
          <?php include_once('partials/left_user_prodile.php');  ?>
          <!-- /menu profile quick info -->

          <br />

          <!-- sidebar menu -->
          <?php include_once('partials/left_menu.php');  ?>
          <!-- /sidebar menu -->

          <!-- /menu footer buttons -->
          <?php include_once('partials/left_buttom_menu.php');  ?>
          <!-- /menu footer buttons -->

        </div>
      </div>

      <!-- top navigation -->
      <?php include_once('partials/right_menu.php');  ?>
      <!-- /top navigation -->

      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>Add Invoice<small></small></h3>
            </div>

            <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="Search for...">
                  <span class="input-group-btn">
                    <button class="btn btn-default" type="button">Go!</button>
                  </span>
                </div>
              </div>
            </div>
          </div>

          <div class="clearfix"></div>

          <div class="row">
            <!-- <h3><?php // echo $message ?></h3> -->
          </div>

          <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">

              <div class="x_panel">
                <div class="x_title">
                  <h2>Product List</h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Product Name</th>
                        <th>Action</th>
                        <th>Qty In Box</th>
                        <th>Category</th>
                        <th>Sub Category</th>
                        <th>Generic</th>
                      </tr>
                    </thead>

                    <tbody>

                      <?php
                      $i = 1;
                      while ($row = mysqli_fetch_array($resultDataList)) {

                        $quary = "SELECT * FROM `category` where id = " . $row['product_type'] . " and store_id = " . $store_id;
                        $resultCatName = mysqli_query($conn, $quary);
                        $catname = "Unknown";
                        while ($ro = mysqli_fetch_array($resultCatName)) {
                          $catname = $ro['cat_name'];
                        }
                        ?>

                        <tr>
                          <td><?php echo $i++ ?></td>
                          <td><?php echo $row['product_name'] ?></td>
                          <td>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#Modal_addToCard" onclick="changeProductInfo('<?php echo $row['id']; ?>', '<?php echo $row['product_name']; ?>')">Add To List</button>
                          </td>
                          <td><?php echo $row['qty_in_box'] ?></td>
                          <td><?php echo $catname; ?></td>
                          <td><?php echo $row['product_subtype'] ?></td>
                          <td><?php echo $row['generic'] ?></td>
                        </tr>
                      <?php   }  ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <!-- End Table -->

            <!-- Card Start  -->

            <div class="col-md-6 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Invoice</h2>

                  <div class="clearfix"></div>
                </div>


                <div class="row">
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                      <div class="x_title">
                        <h2>Selected Product</small></h2>
                        <div class="clearfix"></div>
                      </div>
                      <div class="x_content">
                        <table class="table table-hover">
                          <thead>
                            <tr>
                              <th>#</th>
                              <th>Name</th>
                              <th>Qty</th>
                              <th>Qty As</th>
                              <!-- <th>Discount (%)</th> -->
                            </tr>
                          </thead>
                          <tbody>

                            <?php
                            for ($x = 0; $x < count($selectedData); $x++) {  ?>
                              <tr>
                                <th scope="row"><?php echo $x + 1; ?></th>
                                <td><?php echo $selectedData[$x][0]; ?></td>
                                <td><?php echo $selectedData[$x][1]; ?></td>
                                <td><?php echo $selectedData[$x][2]; ?></td>
                                <!-- <td><?php //echo $selectedData[$x][4]; ?></td> -->
                                <td>
                                  <form id="demo-form2" method="POST" action="<?php echo $_SERVER["PHP_SELF"]; ?>" enctype="multipart/form-data" role="form" data-parsley-validate class="form-horizontal form-label-left">
                                    <input type="hidden" id="productId" name="productId" value="<?php echo $selectedData[$x][3]; ?>">
                                    <button type="Submit" name="removeToCard" class="btn btn-danger">-</button>

                                  </form>
                                </td>
                              </tr>
                            <?php } ?>
                          </tbody>
                        </table>

                      </div>
                    </div>
                  </div>
                </div>


                <div class="x_content">
                  <br />
                  <form id="demo-form2" method="POST" action="<?php echo $_SERVER["PHP_SELF"]; ?>" enctype="multipart/form-data" role="form" data-parsley-validate class="form-horizontal form-label-left">
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3">Invoice No: *</label>
                      <div class="col-md-9 col-sm-9 col-xs-9">
                        <input type="text" id="first-name" required="required" name="invoice_no" class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3">Customer Name: * </label>
                      <div class="col-md-9 col-sm-9 col-xs-9">
                        <input type="text" id="first-name" required="required" name="customer_name" class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3">Tran Type</label>
                      <div class="col-md-9 col-sm-9 col-xs-9">
                        <select id="invoice_type" name="invoice_type" class="form-control" required>
                          <option value="Receive From Supplier">Receive From Supplier</option>
                          <option value="Return to Supplier">Return to Supplier</option>
                          <option value="Sell To Customer">Sell To Customer</option>
                          <option value="Customer Return">Customer Return</option>
                        </select>
                      </div>
                    </div>
                    <!-- <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3">SubTotal</label>
                      <div class="col-md-9 col-sm-9 col-xs-9">
                        <input type="number" min="0" id="first-name" required="required" name="product_name" value="1278" class="form-control col-md-7 col-xs-12" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3">Discount</label>
                      <div class="col-md-9 col-sm-9 col-xs-9">
                        <input type="number" min="0" id="first-name" required="required" name="product_name" class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3">Sub Total</label>
                      <div class="col-md-9 col-sm-9 col-xs-9">
                        <input type="number" min="0" id="first-name" required="required" name="product_name" value="1278" class="form-control col-md-7 col-xs-12" readonly>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3">Paid</label>
                      <div class="col-md-9 col-sm-9 col-xs-9">
                        <input type="number" min="0" id="first-name" required="required" name="product_name" class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3">Due</label>
                      <div class="col-md-9 col-sm-9 col-xs-9">
                        <input type="number" min="0" id="first-name" required="required" name="product_name" value="1278" class="form-control col-md-7 col-xs-12" readonly>
                      </div>
                    </div> -->
                    <div class="ln_solid"></div>

                    <div class="form-group">
                      <div class="col-md-9 col-md-offset-3">
                        <!-- <button type="submit" class="btn btn-primary">Cancel</button> -->
                        <button type="submit" name="saveInvoice" class="btn btn-success">Add</button>
                      </div>
                    </div>

                  </form>
                </div>
              </div>
            </div>

            <!-- card End  -->
          </div>
        </div>
      </div>
      <!-- /page content -->

      <!--  mode start Modal_addToCard  -->

      <div class="modal fade bs-example-modal-sm" id="Modal_addToCard" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm">
          <div class="modal-content">

            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
              </button>
              <h4 class="modal-title" id="myModalLabel2">Add Product </h4>
            </div>
            <form id="demo-form2" method="POST" action="<?php echo $_SERVER["PHP_SELF"]; ?>" enctype="multipart/form-data" role="form" data-parsley-validate class="form-horizontal form-label-left">

              <div class="modal-body">
                <div class="form-group">
                  <label class="col-md-12 col-sm-12 col-xs-12" for="productName1">Product
                    Name
                  </label>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <input type="text" id="productName1" required="required" name="productName" class="form-control col-md-7 col-xs-12" readonly>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-md-12 col-sm-12 col-xs-12" for="productAmount"> Qty <span class="required">*</span>
                  </label>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <input type="number" id="productAmount" value="0" name="productAmount" class="form-control col-md-7 col-xs-12" required="required">
                  </div>
                </div>

                <div class="form-group clearfix">
                  <label class="col-md-12 col-sm-12 col-xs-12" for="product_buy_type"> Qty As <span class="required">*</span>
                  </label>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <select id="product_buy_type" name="product_buy_type" class="form-control" required>
                      <option value="Box">Box</option>
                      <option value="Pcs">Pcs</option>
                    </select> </div>
                </div>

                <div class="form-group">
                  <label class="col-md-12 col-sm-12 col-xs-12" for="productDiscount"> Discount (percent) <span class="required">*</span>
                  </label>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <input type="number" id="productDiscount" value="0" name="productDiscount" class="form-control col-md-7 col-xs-12" required="required">
                  </div>
                </div>


                <input type="hidden" id="productId1" name="productId">
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" name="addToCard" class="btn btn-primary">Add</button>
              </div>
            </form>
          </div>
        </div>
      </div>

      <!-- modal end  -->



      <!-- footer content -->
      <?php include_once('partials/footer.php');  ?>
      <!-- /footer content -->
    </div>
  </div>

  <!-- jQuery -->
  <script src="public/vendors/jquery/dist/jquery.min.js"></script>
  <!-- Bootstrap -->
  <script src="public/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- FastClick -->
  <script src="public/vendors/fastclick/lib/fastclick.js"></script>
  <!-- NProgress -->
  <script src="public/vendors/nprogress/nprogress.js"></script>
  <!-- iCheck -->
  <script src="public/vendors/iCheck/icheck.min.js"></script>
  <!-- Datatables -->
  <script src="public/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="public/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
  <script src="public/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
  <script src="public/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
  <script src="public/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
  <script src="public/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
  <script src="public/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
  <script src="public/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
  <script src="public/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
  <script src="public/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
  <script src="public/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
  <script src="public/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
  <script src="public/vendors/jszip/dist/jszip.min.js"></script>
  <script src="public/vendors/pdfmake/build/pdfmake.min.js"></script>
  <script src="public/vendors/pdfmake/build/vfs_fonts.js"></script>


  <!-- Custom Theme Scripts -->
  <script src="public/build/js/custom.min.js"></script>


  <script>
    function changeProductInfo(id, name) {
      // alert(id);
      document.getElementById("productName1").value = name;
      document.getElementById("productId1").value = id;
      document.getElementById("productAmount").value = 1;

    }
  </script>
  <?php include_once('partials/notification.php');  ?>
  
</body>

</html>