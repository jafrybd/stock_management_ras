<?php

session_start();


if (isset($_SESSION['store_management_user_id'])) {
  $store_id = $_SESSION['store_management_store_id'];
  $store_name = $_SESSION['store_management_store_name'];

  $invId = -1;
  $totalItem = 0;
  $totalQty = 0;
  $cusName = "";
  $invDate = "";
  $inv_no = "";
  $tranType = "";
  $sub_total = $total = $due = $paid = $discount = 0;
  if (isset($_GET['id'])) {
    $invId = $_GET['id'];
  }

  include_once('controller/addInvoiceToCard.php');

  $quary = "SELECT * FROM `invoice_tran` WHERE id = " . $invId . " and store_id = " . $store_id;
  $resultInvoiceTranList = mysqli_query($conn, $quary);

  while ($ro = mysqli_fetch_array($resultInvoiceTranList)) {
    $cusName = $ro['customer_name'];
    $invDate = date_format(date_create($ro['creating_date']),"d M, Y");
    $inv_no = $ro['inv_no'];
    $tranType = $ro['tran_type'];
    $sub_total = $ro['sub_total'];
    $total = $ro['total'];
    $due = $ro['due'];
    $paid = $ro['paid'];
    $discount = $ro['discount'];
  }

  $quary = "SELECT * FROM `invoice_tran_item` WHERE  	invoice_id = " . $invId . " and store_id = " . $store_id;
  $resultInvoiceTranItemList = mysqli_query($conn, $quary);
} else {
  header("location: login.php");
}

?>


<!DOCTYPE html>
<html lang="en">

<head>
  <?php include_once('partials/header_common.php');  ?>
</head>

<body class="nav-md">
  <div class="container body">
    <div class="main_container">
      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">


          <!-- menu profile quick info -->
          <!-- <% include partials/left_user_prodile.ejs %> -->
          <?php include_once('partials/left_user_prodile.php');  ?>
          <!-- /menu profile quick info -->

          <br />

          <!-- sidebar menu -->
          <!-- <% include partials/left_menu.ejs %> -->
          <?php include_once('partials/left_menu.php');  ?>
          <!-- /sidebar menu -->

          <!-- /menu footer buttons -->
          <?php include_once('partials/left_buttom_menu.php');  ?>
          <!-- /menu footer buttons -->
        </div>
      </div>

      <!-- top navigation -->
      <!-- <% include partials/right_menu.ejs %> -->
      <?php include_once('partials/right_menu.php');  ?>
      <!-- /top navigation -->

      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3> <small> </small></h3>
            </div>

            <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="Search for...">
                  <span class="input-group-btn">
                    <button class="btn btn-default" type="button">Go!</button>
                  </span>
                </div>
              </div>
            </div>
          </div>

          <div class="clearfix"></div>

          <div class="row">
            <div class="col-md-12  col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Invoice <small> </small></h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <section class="content invoice">
                    <!-- title row -->
                    <div class="row">
                      <div class="col-xs-12 invoice-header">
                        <h1>
                          <i class="fa fa-globe"></i> Invoice.
                          <small class="pull-right">Date: <?php echo $invDate; ?></small>
                        </h1>
                      </div>
                      <!-- /.col -->
                    </div>
                    <!-- info row -->
                    <div class="row invoice-info">
                      <div class="col-sm-4 invoice-col">
                        From
                        <address> <br>
                          <strong><?php echo $store_name; ?></strong>
                          <br>Phone: 01670--145
                        </address>
                      </div>
                      <!-- /.col -->
                      <div class="col-sm-4 invoice-col">
                        To
                        <address> <br>
                          <strong><?php echo $cusName; ?></strong>
                          <!-- <br> Phone: 01923-9876 -->
                        </address>
                      </div>
                      <!-- /.col -->
                      <div class="col-sm-4 invoice-col">
                        <b>Invoice No: <?php echo $inv_no; ?></b><br>
                        <b>Invoice Type: <?php echo  $tranType; ?></b><br>

                        <!-- <br>
                        <b>Order ID:</b> 4F3S8J
                        <br> 
                        <b>Date:</b> 99-2-33 <br>
                        <b>Account:</b> 968-34567 -->
                      </div>
                      <!-- /.col -->
                    </div>
                    <!-- /.row -->

                    <!-- Table row -->
                    <div class="row" style="padding-top: 30px">
                      <div class="col-xs-12 table">
                        <table class="table table-striped">
                          <thead>
                            <tr>
                              <th>No</th>
                              <th style="width: 15%">Name</th>
                              <th>Qty</th>
                              <th>Per Rate</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php
                            $i = 1;
                            while ($row = mysqli_fetch_array($resultInvoiceTranItemList)) {
                              $totalItem++;
                              $totalQty += $row['qty'];
                              $quary = "SELECT * FROM `product` where id = " . $row['product_id'] . " and store_id = " . $store_id;
                              $product_name = "Unknown";
                              $resultCompanyName = mysqli_query($conn, $quary);
                              while ($ro = mysqli_fetch_array($resultCompanyName)) {
                                $product_name = $ro['product_name'];
                              }
                            ?>
                              <tr>
                                <td><?php echo $i++ ?></td>
                                <td><?php echo $product_name; ?></td>
                                <td><?php echo $row['qty']; ?></td>
                                <td><?php echo $row['price_per_rate']; ?></td>
                              </tr>
                            <?php   }  ?>
                          </tbody>
                        </table>
                      </div>
                      <!-- /.col -->
                    </div>
                    <!-- /.row -->

                    <div class="row">
                      <!-- accepted payments column -->
                      <div class="col-xs-6">
                        <p class="lead">Summary:</p>

                        <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                          Total Item: <?php echo $totalItem; ?> <br>
                          Total Qty: <?php echo $totalQty; ?> product
                        </p>
                      </div>
                      <!-- /.col -->
                      <div class="col-xs-6">
                        <p class="lead">Invoice Payment</p>
                        <div class="table-responsive">
                          <table class="table">
                            <tbody>
                              <tr>
                                <th style="width:50%">Sub Total:</th>
                                <td><?php echo $sub_total; ?> Tk</td>
                              </tr>
                              <tr>
                                <th>Discount </th>
                                <td><?php echo $discount; ?> Tk</td>
                              </tr>
                              <tr>
                                <th>Net Total:</th>
                                <td><?php echo $total; ?> Tk</td>
                              </tr>
                              <tr>
                                <th>Paid</th>
                                <td><?php echo $paid; ?> Tk</td>
                              </tr>
                              <tr>
                                <th>Due</th>
                                <td><?php echo $due; ?> Tk</td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                      <!-- /.col -->
                    </div>
                    <!-- /.row -->

                    <!-- this row will not appear when printing -->
                    <!-- <div class="row no-print">
                      <div class="col-xs-12">
                        <button class="btn btn-default" onclick="window.print();"><i class="fa fa-print"></i> Print</button>
                        <button class="btn btn-primary pull-right" style="margin-right: 5px;"><i class="fa fa-download"></i> Generate PDF</button>
                      </div>
                    </div> -->
                  </section>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /page content -->

      <!-- footer content -->
      <footer>
        <div class="pull-right">
          Power By Rename Tech
        </div>
        <div class="clearfix"></div>
      </footer>
      <!-- /footer content -->
    </div>
  </div>

  <!-- jQuery -->
  <script src="public/vendors/jquery/dist/jquery.min.js"></script>
  <!-- Bootstrap -->
  <script src="public/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- FastClick -->
  <script src="public/vendors/fastclick/lib/fastclick.js"></script>
  <!-- NProgress -->
  <script src="public/vendors/nprogress/nprogress.js"></script>

  <!-- Custom Theme Scripts -->
  <script src="public/build/js/custom.min.js"></script>

  <?php include_once('partials/notification.php');  ?>
</body>

</html>