<?php

include_once('./connection.php');
include_once('common.php');
$obj = new Common();

function test_input($data)
{
	$data = trim($data);
	$data = stripslashes($data);
	return $data;
}

$error_text_color = "red";
$error_point = 0;
$notification = 0;

if ($_SERVER["REQUEST_METHOD"] == "POST") {
	if (isset($_POST['AddProduct']) || isset($_POST['ProductDetailsUpdate'])) {

		// company name 
		if (empty($_POST["product_name"])) {
			$error_point++;
		} else {
			$product_name = test_input($_POST["product_name"]);
		}

		// product category 
		if (empty($_POST["product_category"])) {
			$error_point++;
		} else {
			$product_category = test_input($_POST["product_category"]);
		}

		//  product_sub_category 
		if (empty($_POST["product_sub_category"])) {
			$error_point++;
		} else {
			$product_sub_category = test_input($_POST["product_sub_category"]);
		}

		// product product_generic 
		if (empty($_POST["product_generic"])) {
			$error_point++;
		} else {
			$product_generic = test_input($_POST["product_generic"]);
		}

		// product product_company 
		if (empty($_POST["product_company"])) {
			if (isset($_POST['AddProduct'])) {
				$error_point++;
			}
		} else {
			$product_company = test_input($_POST["product_company"]);
		}

		// product qty_in_box 
		if (empty($_POST["qty_in_box"])) {
			$error_point++;
		} else {
			$qty_in_box = test_input($_POST["qty_in_box"]);
			if ($qty_in_box == 0) {
				$qty_in_box = 1;
			}
		}

		// product Buing price in_box 
		if (empty($_POST["buying_price_in_box"])) {
			if (isset($_POST['AddProduct'])) {
				$error_point++;
			}
		} else {
			$buying_price_in_box = test_input($_POST["buying_price_in_box"]);
			if ($buying_price_in_box == 0) {
				$buying_price_in_box = 1;
			}
		}

		// product saleing price in_box 
		if (empty($_POST["saleing_price_in_box"])) {
			if (isset($_POST['AddProduct'])) {
				$error_point++;
			}
		} else {
			$saleing_price_in_box = test_input($_POST["saleing_price_in_box"]);
			if ($saleing_price_in_box == 0) {
				$saleing_price_in_box = 1;
			}
		}


		// discription
		if (empty($_POST["discription"])) {
			// $error_point++;
			$discription = "";
		} else {
			$discription = test_input($_POST["discription"]);
		}

		// visibility
		$visibility = 0;
		if (!empty($_POST["form_visibility"])) {
			$visibility = 1;
		}


		// Check Data
		$checkDuplicat = $obj->checkDataExisting('product', 'product_name', $product_name, $store_id, $conn);


		if ($error_point > 0) {
			$error_point = 0;
			$message = "Please Fill up all input field " . $error_point;
			// $message = "insert into `product` (product_name, product_type, product_subtype, generic, qty_in_box, company_id, description,visibility, store_id) VALUES ('" . $product_name . "','" . $product_category . "','" . $product_sub_category . "', '" . $product_generic . "', " . $qty_in_box . "," . $product_company . " ,'" . $discription . "', " . $visibility . ", " . $store_id . ")";

			$notification = 2;
		} else if (($checkDuplicat > 0 && isset($_POST['AddProduct'])) || ($checkDuplicat > 1 && isset($_POST['ProductDetailsUpdate']))) {
			$message = 'Product Already Added';
			$notification = 2;
		} else {

			if (isset($_POST['AddProduct'])) {
				$quary = "insert into `product` (product_name, product_type, product_subtype, generic, qty_in_box, company_id, description,visibility, store_id) VALUES ('" . $product_name . "','" . $product_category . "','" . $product_sub_category . "', '" . $product_generic . "', " . $qty_in_box . "," . $product_company . " ,'" . $discription . "', " . $visibility . ", " . $store_id . ")";
				$message = 'Product Added Successfully Done ';
			} else {
				$id = test_input($_POST["productId"]);
				$quary = "UPDATE `product` set product_name = '" . $product_name . "', product_type = '" . $product_category . "',  product_subtype = '" . $product_sub_category . "',  generic = '" . $product_generic . "',  qty_in_box = " . $qty_in_box . ",  description = '" . $discription . "', company_id = ". $product_company .",  visibility = " . $visibility . " where id = " . $id . " and store_id = " . $store_id;
				$message = 'Product Data Update Successfully Done';
			}
			$notification = 1;

			if ($conn->query($quary) === False) {
				$message = 'Fail as : ' . $conn->$quary;
				$notification = 3;
			} else {
				if (isset($_POST['AddProduct'])) {
					$quary = "insert INTO `stock` (product_id, product_name, buing_price, sell_price, store_id) VALUES ('" . $conn->insert_id . "','" . $product_name . "'," . $buying_price_in_box . "," . ($saleing_price_in_box / $qty_in_box) . " , " . $store_id . ")";
					if ($conn->query($quary) === False) {
						$message = 'Fail as : ' . $conn->$quary;
						$notification = 3;
					}
				}
			}
		}
	} elseif (isset($_POST['productDelete'])) {
		$id = test_input($_POST["deleteProductId"]);
		$quary = "DELETE FROM `product` where id = " . $id . " and store_id = " . $store_id;
		if ($conn->query($quary) === TRUE) {
			$message = 'Product Delete Successfully Done';
			$notification = 1;
		} else {
			$message = 'Fail as : ' . $conn->$quary;
			$notification = 3;
		}
	} else {
		$message = "Unknow Request";
		$notification = 3;
	}
} else {
	$message  = "";
}
