<?php

include_once('./connection.php');

function test_input($data)
{
	$data = trim($data);
	$data = stripslashes($data);
	return $data;
}

function checkDataExisting($tableName, $rowName, $date, $storeId, $conn)
{
	$quary = "SELECT COUNT(*) as num FROM `" . $tableName . "` where " . $rowName . " = '" . $date . "' and store_id = " . $storeId;
	$result = mysqli_query($conn, $quary);
	$data = mysqli_fetch_assoc($result);
	return $data['num'];
}

$error_text_color = "red";
$error_point = 0;
$notification = 0 ;
$message = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
	if (isset($_POST['addGeneric']) || isset($_POST['GenericDetailsUpdate'])) {

		// company name 
		if (empty($_POST["form_generic_name"])) {
			$error_point++;
		} else {
			$generic_name = test_input($_POST["form_generic_name"]);
		}

		// company description
		if (empty($_POST["form_generic_description"])) {
			$error_point++;
		} else {
			$generic_description = test_input($_POST["form_generic_description"]);
		}

		// Check Data

		$checkDuplicat = checkDataExisting('generic', 'generic_name', $generic_name, $store_id, $conn);

		if ($error_point > 0) {
			$error_point = 0;
			$notification = 2 ;
			$message = 'Please Fill up all input field';
		} else if (($checkDuplicat > 0 && isset($_POST['addGeneric'])) || ($checkDuplicat > 1 && isset($_POST['GenericDetailsUpdate'])) ) {
			$message = 'Generic Already Added';
			$notification = 2 ;
		} else {

			if (isset($_POST['addGeneric'])) {
				$quary = "insert into `generic` (generic_name, generic_description, store_id) VALUES ('" . $generic_name . "','" . $generic_description . "', " . $store_id . ")";
				$message = 'Generic Added Successfully Done';
			} else {
				$id = test_input($_POST["genericId"]);
				$quary = "UPDATE `generic` set generic_name = '" . $generic_name . "', generic_description = '" . $generic_description . "' where id = " . $id . " and store_id = " . $store_id;
				$message = 'Generic Update Successfully Done';
			}


			if ($conn->query($quary) === False) {
				$message = 'Fail as : ' . $conn->$quary;
				$notification = 3 ;
			} else $notification = 1 ;
		}
	} elseif (isset($_POST['GenericDelete'])) {
		$id = test_input($_POST["deleteGenericId"]);
		$quary = "DELETE FROM `generic` where id = " . $id . " and store_id = " . $store_id;
		if ($conn->query($quary) === TRUE) {
			$message = 'Company Delete Successfully Done';
			$notification = 1 ;
		} else {
			$message = 'Fail as : ' . $conn->$quary;
			$notification = 3 ;
		}
	} else $message = "Unknow Request";
} else {
	$message  = "";
}
