<?php

include_once('./connection.php');

function test_input($data)
{
	$data = trim($data);
	$data = stripslashes($data);
	return $data;
}

$error_point = 0;
$notification = 0 ;

if ($_SERVER["REQUEST_METHOD"] == "POST") {
	if (isset($_POST['addSubCategory']) || isset($_POST['ProductDetailsUpdate'])) {

		// company name 
		if (empty($_POST["form_subCategory_name"])) {
			$error_point++;
		} else {
			$sub_cat_name = test_input($_POST["form_subCategory_name"]);
		}

		// company description
		if (empty($_POST["form_subCategory_description"])) {
			$error_point++;
		} else {
			$sub_cat_description = test_input($_POST["form_subCategory_description"]);
		}


		// Check Data

		if ($error_point > 0) {
			$error_point = 0;
			$message = "Please Fill up all input field";
			$notification = 2 ;
		} else {

			if (isset($_POST['addSubCategory'])) {
				$quary = "INSERT into `category` (cat_name, cat_description, store_id, sub_cat_id) VALUES ('" . $sub_cat_name . "', '" . $sub_cat_description . "', " . $store_id . ", ".$catId.");";
				$message = 'Company Added Successfully Done';
				$notification = 1 ;
			} else {
				$id = test_input($_POST["subcategoryId"]);
				$quary = "UPDATE `category` set cat_name = '" . $sub_cat_name ."' , cat_description = '". $sub_cat_description ."' where store_id = " . $store_id ." and id = " .$id;
				$message = 'Company Data Update Successfully Done ';
				$notification = 1 ;
			}
			

			if ($conn->query($quary) === False) {
				$message = 'Operation Fail ';
				$notification = 3 ;
			}

			// else {
			// 	$path = "sub_category_list.php?id=" .$catId;
			//     header("location: $path");
			// }
			
		}

	} elseif (isset($_POST['subCatDelete'])) {
		$id = test_input($_POST["deleteSubCatId"]);
		$quary = "DELETE FROM `category` where id = " . $id . " and store_id = " . $store_id;
		if ($conn->query($quary) === TRUE) {
			$message = 'Company Delete Successfully Done';
			$notification = 1 ;
		} else {
			$message = 'Fail as : ';
			$notification = 3 ;
		}

		$path = "sub_category_list.php?id=" .$catId;
			    header("location: $path");
	} 
	
	else $message = "Unknow Request";

} else {
	$message  = "";
}
