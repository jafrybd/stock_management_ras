<?php

include_once('./connection.php');
$page = "invoice";

function test_input($data)
{
	$data = trim($data);
	$data = stripslashes($data);
	return $data;
}

function updateStockAndGetQty($conn, $productId, $productQty, $callType, $invoice_type, $store_id)
{

	// return "";
	$qty = $productQty;
	$totalQty = 0;
	date_default_timezone_set("Asia/Dhaka");
	$nowDateAndTime = date("Y-m-d H-i-s");

	if ($callType == "Box") {
		$query = "SELECT * FROM `product` where id = " . $productId . " and store_id = " . $store_id;
		$result = $conn->query($query);
		// return $query;
		$totalQty = 1;
		while ($row = mysqli_fetch_array($result)) {
			$totalQty = $row['qty_in_box'];
			break;
		}
		$qty *= $totalQty;
	}

	$query = "SELECT * FROM `stock` where product_id = " . $productId . " and store_id = " . $store_id;
	$result = $conn->query($query);
	$current_stock = 0;
	while ($row = mysqli_fetch_array($result)) {
		$current_stock = $row['current_stock'];
		break;
	}
	$totalQty = $qty;

	if ($invoice_type == "Receive From Supplier" || $invoice_type == "Customer Return") {
		$qty += $current_stock;
	} else if ($invoice_type == "Return to Supplier" || $invoice_type == "Sell To Customer") {
		$qty = $current_stock - $qty;
	}


	$query = "UPDATE `stock` set current_stock = '" . $qty . "', last_tran_date = '" . $nowDateAndTime . "'where store_id = " . $store_id . " and product_id = " . $productId;
	$result = $conn->query($query);

	return $totalQty;
}


function addDues($conn, $customer_name, $invoice_no, $tran_type, $amount) {
	$query = "INSERT INTO `dues` (customer_name, invoice_no, tran_type, amount) VALUES ('".$customer_name ."', ".$invoice_no.", '".$tran_type."', ".$amount." )";
	$result = $conn->query($query);
}

$error_text_color = "red";
$error_point = 0;
$tran = "";
$notification = 0;

if ($_SERVER["REQUEST_METHOD"] == "POST") {

	if (isset($_POST['addToCard'])) {

		// product name 
		if (empty($_POST["productName"])) {
			$error_point++;
			$tran = $tran . "1 ";
		} else {
			$productName = test_input($_POST["productName"]);
		}

		// productAmount
		if (empty($_POST["productAmount"])) {
			$error_point++;
			$tran = $tran . "2 ";
		} else {
			$productAmount = test_input($_POST["productAmount"]);
		}

		// product_buy_type
		if (empty($_POST["product_buy_type"])) {
			$tran = $tran . "3 ";
			$error_point++;
		} else {
			$product_buy_type = test_input($_POST["product_buy_type"]);
		}

		// productId
		if (empty($_POST["productId"])) {
			$error_point++;
		} else {
			$productId = test_input($_POST["productId"]);
		}

		// Discount
		if (!isset($_POST["productAmount"])) {
			$productDiscount = 0;
		} else {
			$productDiscount = test_input($_POST["productDiscount"]);
		}


		// sell Price
		if (isset($_POST["sellPrice"])) {
			$sellPrice = test_input($_POST["sellPrice"]);
			$page = "sell";
		} else {
			$sellPrice = 0;
		}


		// Buing Price
		if (!isset($_POST["buingPrice"])) {
			$buingPrice = 0;
		} else {
			$buingPrice = test_input($_POST["buingPrice"]);
		}

		// Qty in Box
		if (!isset($_POST["qty_in_box"])) {
			$qty_in_box = 1;
		} else {
			$qty_in_box = test_input($_POST["qty_in_box"]);
		}




		// Check Data

		if ($error_point > 0) {
			$message = "Please Fill up all input field : ";
			$error_point = 0;
			$notification = 2;
		} else {

			$arrayExist = $_SESSION["stockManagement_SelectedProductList"];
			$newArray = array($productName, $productAmount, $product_buy_type, $productId, $productDiscount, $buingPrice, $sellPrice, $qty_in_box);

			$message = 'Already Add to the card';
			$notification = 2;

			$isAdded = 0;
			for ($x = 0; $x < count($arrayExist); $x++) {
				if ($arrayExist[$x][3] == $productId) {
					$isAdded = 1;
					break;
				}
			}

			if ($isAdded == 0) {
				array_push($arrayExist, $newArray);
				$_SESSION["stockManagement_SelectedProductList"] = $arrayExist;
				$notification = 1;

				if ($page == 'sell') {
					header("location: add_sells_invoice.php");
				} else {
					header("location: add_invoice.php");
				}
			}
		}
	} else if (isset($_POST['removeToCard'])) {
		// productId
		if (empty($_POST["productId"])) {
			$error_point++;
		} else {
			$productId = test_input($_POST["productId"]);
		}

		$message = "";
		$arrayExist = $_SESSION["stockManagement_SelectedProductList"];



		for ($x = 0; $x < count($arrayExist); $x++) {
			if ($arrayExist[$x][3] == $productId) {
				// unset($arrayExist[$x]);
				array_splice($arrayExist, $x, 1);
			}
		}

		$_SESSION["stockManagement_SelectedProductList"] = $arrayExist;
		// } else if (isset($_POST['saveInvoice'])) {
	} else {

		$selectedData = $_SESSION["stockManagement_SelectedProductList"];
		$message  = "";


		if (count($selectedData) > 0) {

			if (empty($_POST["invoice_type"])) {
				$error_point++;
			} else {
				$invoice_type = test_input($_POST["invoice_type"]);
			}

			if (empty($_POST["customer_name"])) {
				// $error_point++;
				$customer_name = "Un Known";
			} else {
				$customer_name = test_input($_POST["customer_name"]);
			}

			if (empty($_POST["invoice_no"])) {
				$error_point++;
			} else {
				$invoice_no = test_input($_POST["invoice_no"]);
			}

			if (!isset($_POST["inv_subtotal"])) {
				$inv_subtotal = 0;
			} else {
				$inv_subtotal = test_input($_POST["inv_subtotal"]);
			}

			if (!isset($_POST["inv_discount"])) {
				$inv_discount = 0;
			} else {
				$inv_discount = test_input($_POST["inv_discount"]);
			}

			if (!isset($_POST["inv_total"])) {
				$inv_total = 0;
			} else {
				$inv_total = test_input($_POST["inv_total"]);
			}

			if (!isset($_POST["inv_paid"])) {
				$inv_paid = 0;
			} else {
				$inv_paid = test_input($_POST["inv_paid"]);
			}

			if (!isset($_POST["inv_due"])) {
				$inv_due = 0;
			} else {
				$inv_due = test_input($_POST["inv_due"]);
			}

			// array($productName, $productAmount, $product_buy_type, $productId);

			$notification = 1;
			$message = "Invoice Add Successfully Done.";
			$query = "insert INTO `invoice_tran` (inv_no, tran_type, customer_name, sub_total, total, due, paid, discount, inv_creator_id, store_id) VALUES ('" . $invoice_no . "','" . $invoice_type . "','" . $customer_name . "' ," . $inv_subtotal . ", " . $inv_total . ", " . $inv_due . ", " . $inv_paid . ", " . $inv_discount . ", ".$invoiceCreatorId.", " . $store_id . ")";

			$conn2 = $conn;
			if ($conn->query($query) === true) {
				$newInvoiceId = $conn->insert_id;
				// $newArray = array($productName, $productAmount, $product_buy_type, $productId, $productDiscount, $buingPrice, $sellPrice, $qty_in_box);

				for ($x = 0; $x < count($selectedData); $x++) {
					$qty = updateStockAndGetQty($conn2, $selectedData[$x][3],  $selectedData[$x][1],  $selectedData[$x][2], $invoice_type, $store_id);
					$query = "insert INTO `invoice_tran_item` (invoice_id, tran_type, product_id, price_per_rate, discount, qty, store_id) VALUES (" . $newInvoiceId . ", '" . $invoice_type . "'," . $selectedData[$x][3] . " ," . $selectedData[$x][6] . ", " . $selectedData[$x][4] . ", " . $qty . ", " . $store_id . ")";

					if ($conn2->query($query) === false) {
						$message = 'Fail as 2 : ';
						$notification = 3;
						break;
					} else {
						// $message = '' . $selectedData[$x][4];

						if($inv_due>=1) {
							addDues($conn, $customer_name, $newInvoiceId, $invoice_type, $inv_due);
						}
						
					}
				}
			} else {
				$message = 'Fail as 1: '.$invoiceCreatorId;
				$notification = 3;
			}

			$_SESSION['stockManagement_SelectedProductList'] = array();
		} else {
			$message  = "No item is Selected, Fast Select Some item";
			$notification = 2;
		}
	}
	// else {
	// 	$message  = "Unknown no set";
	// 	$notification = 3;
	// }
} else {
	$message  = "Unknown";
	$notification = 4;
}
