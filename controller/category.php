<?php

include_once('./connection.php');
include_once('common.php');
$obj = new Common();

function test_input($data)
{
	$data = trim($data);
	$data = stripslashes($data);
	return $data;
}

$error_text_color = "red";
$error_point = 0;
$notification = 0 ;

if ($_SERVER["REQUEST_METHOD"] == "POST") {

	if (isset($_POST['catUpdate']) || isset($_POST['addCategory'])) {

		// company name 
		if (empty($_POST["category_name"])) {
			$error_point++;
		} else {
			$cat_name = test_input($_POST["category_name"]);
		}

		// company description
		if (empty($_POST["category_discription"])) {
			$error_point++;
		} else {
			$cat_description = test_input($_POST["category_discription"]);
		}


		// Check Data
		$checkDuplicat = $obj-> checkDataExisting('category', 'cat_name', $cat_name, $store_id, $conn);

		if ($error_point > 0) {
			$error_point = 0;
			$message = "Please Fill up all input field";
			$notification = 2 ;

		} else if (($checkDuplicat > 0 && isset($_POST['addCategory'])) || ($checkDuplicat > 1 && isset($_POST['catUpdate'])) ) {
			$message = 'Category Already Added';
			$notification = 2 ;
		} else {

			if (isset($_POST['addCategory'])) {
				$quary = "INSERT into `category` (cat_name, cat_description, sub_cat_id, store_id) VALUES ('" . $cat_name . "', '" . $cat_description . "', 0, " . $store_id . ");";
				$message = 'Category Added Successfully Done';
				$notification = 1 ;
			} else {
				$id = test_input($_POST["category_id"]);
				$quary = "UPDATE `category` set cat_name = '" . $cat_name ."' , cat_description = '". $cat_description ."' where store_id = " . $store_id ." and id = " .$id;
				$message = 'Company Data Update Successfully Done';
				$notification = 1 ;
			}

			if ($conn->query($quary) === False) {
				$message = 'Fail as Unknow Reason ';
				$notification = 3 ;
			}
		}
	}
} else {
	$message  = "";
}
