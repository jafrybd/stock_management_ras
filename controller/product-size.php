<?php

include_once('./connection.php');

function test_input($data)
{
	$data = trim($data);
	$data = stripslashes($data);
	return $data;
}

function checkDataExisting($tableName, $rowName, $date, $storeId, $conn)
{
	$quary = "SELECT COUNT(*) as num FROM `" . $tableName . "` where " . $rowName . " = '" . $date . "' and status = 1 and  store_id = " . $storeId;
	$result = mysqli_query($conn, $quary);
	$data = mysqli_fetch_assoc($result);
	return $data['num'];
}

function checkDataExistingByID($tableName, $rowName, $date, $storeId, $conn, $id = 0)
{
	$quary = "SELECT COUNT(*) as num FROM `" . $tableName . "` where " . $rowName . " = '" . $date . "' and status = 1 and  id = " . $id . " and  store_id = " . $storeId;
	$result = mysqli_query($conn, $quary);
	$data = mysqli_fetch_assoc($result);
	return $data['num'];
}

$error_text_color = "red";
$error_point = 0;
$notification = 0;
$message = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
	if (isset($_POST['addProductSize']) || isset($_POST['ProductSizeDetailsUpdate'])) {

		// Title 
		if (empty($_POST["form_title"])) {
			$error_point++;
		} else {
			$title = test_input($_POST["form_title"]);
			$title = $conn->real_escape_string($title);
		}


		// Check Data

		$checkDuplicat = checkDataExisting('product_size', 'title', $title, $store_id, $conn);

		if ($error_point > 0) {
			$error_point = 0;
			$notification = 2;
			$message = 'Please Fill up all input field';
		} else if (($checkDuplicat > 0 && isset($_POST['addProductSize'])) || ($checkDuplicat > 1 && isset($_POST['ProductSizeDetailsUpdate']))) {
			$message = 'Product Size Already Added';
			$notification = 2;
		} else {

			$can_quesry_run = true;

			if (isset($_POST['addProductSize'])) {
				$quary = "insert into `product_size` (title,  store_id) VALUES ('" . $title . "', " . $store_id . ")";
				$message = 'Product Size Added Successfully Done';
			} else {
				$id = test_input($_POST["productHeadId"]);

				if ($checkDuplicat != 0) {  // understand there is a duplicat value
					$checkDuplicat = checkDataExistingByID('product_size', 'title', $title, $store_id, $conn, $id);
					if ($checkDuplicat != 1) { // duplicat value and request value id is not same
						$can_quesry_run = false;
					}
				}

				if ($can_quesry_run == true) {
					$quary = "UPDATE `product_size` set title = '" . $title . "' where id = " . $id . " and store_id = " . $store_id;
					$message = 'Product Size Update Successfully Done';
				} else { 
					$message = 'Product Size Already Added';
					$notification = 2;
				}
			}

			if ($can_quesry_run == true) {
				if ($conn->query($quary) === False) {
					$message = 'Fail as : ' . $conn->$quary;
					$notification = 3;
				} else $notification = 1;
			}
		}
	} elseif (isset($_POST['GenericDelete'])) {
		$id = test_input($_POST["deleteGenericId"]);
		$quary = "UPDATE `product_size` set status = 0  where id = " . $id . " and store_id = " . $store_id;
		if ($conn->query($quary) === TRUE) {
			$message = 'Product Size Delete Successfully Done';
			$notification = 1;
		} else {
			$message = 'Fail as : ' . $conn->$quary;
			$notification = 3;
		}
	} else $message = "Unknow Request";
} else {
	$message  = "";
}
