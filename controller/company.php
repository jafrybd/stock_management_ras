<?php

include_once('./connection.php');
include_once('common.php');
$obj = new Common();

function test_input($data)
{
	$data = trim($data);
	$data = stripslashes($data);
	return $data;
}

$error_text_color = "red";
$error_point = 0;
$notification = 0;



if ($_SERVER["REQUEST_METHOD"] == "POST") {
	if (isset($_POST['addCompany']) || isset($_POST['ProductDetailsUpdate'])) {

		// company name 
		if (empty($_POST["form_company_name"])) {
			$error_point++;
		} else {
			$company_name = test_input($_POST["form_company_name"]);
		}

		// company description
		if (empty($_POST["form_company_description"])) {
			$error_point++;
		} else {
			$company_description = test_input($_POST["form_company_description"]);
		}


		// visibility
		if (empty($_POST["form_visibility"])) {
			$error_point++;
		} else {
			$visibility = test_input($_POST["form_visibility"]);
		}

		$checkDuplicat = $obj->checkDataExisting('company', 'company_name', $company_name, $store_id, $conn);
		// Check Data
		if ($error_point > 0) {
			$error_point = 0;
			$message = "Please Fill up all input field";
			$notification = 2;
		} else if (($checkDuplicat > 0 && isset($_POST['addCompany'])) || ($checkDuplicat > 1 && isset($_POST['ProductDetailsUpdate'])) ) {
			$message = 'Company Already Added';
			$notification = 2;
		} else {

			if (isset($_POST['addCompany'])) {
				$quary = "insert into `company` (company_name, company_description, visibility, store_id) VALUES ('" . $company_name . "','" . $company_description . "'," . $visibility . ", " . $store_id . ")";
				$message = 'Company Added Successfully Done';
				$notification = 1;
			} else {
				$id = test_input($_POST["companyId"]);
				$quary = "UPDATE `company` set company_name = '" . $company_name . "', company_description = '" . $company_description . "', visibility = " . $visibility . " where id = " . $id . " and store_id = " . $store_id;
				$message = 'Company Data Update Successfully Done';
				$notification = 1;
			}


			if ($conn->query($quary) === False) {
				$message = 'Fail as Unknow Reason ';
				$notification = 2;
			}
		}
	} elseif (isset($_POST['CompanyDelete'])) {
		$id = test_input($_POST["deleteProductId"]);
		$quary = "DELETE FROM `company` where id = " . $id . " and store_id = " . $store_id;
		if ($conn->query($quary) === TRUE) {
			$message = 'Company Delete Successfully Done';
			$notification = 1;
		} else {
			$message = 'Fail as unknow reason ';
			$notification = 3;
		}
	} else {
		$message = "Unknow Request";
		$notification = 3;
	}
} else {
	$message  = "";
}
