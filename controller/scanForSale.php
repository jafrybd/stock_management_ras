<?php
include_once('../connection.php');
session_start();


if (isset($_SESSION['store_management_user_id'])) {
  $store_id = $_SESSION['store_management_store_id'];
}

if (empty($_POST["id"])) {
    $id = -1;
} else {
    $id = ($_POST["id"]);
}


$quary = "SELECT stock.product_id, stock.product_name, product.qty_in_box, stock.sell_price FROM `stock` INNER join `product`where stock.product_id =  product.id and product.id = " . $id . " and  product.store_id = " . $store_id;
$result = mysqli_query($conn, $quary);

if (mysqli_num_rows($result) > 0) {
    $objJSON = mysqli_fetch_array($result);
    $res = array("status" => "success", "objectJson" => $objJSON);
} else {
    $res = array("status" => "error", "message" => "No records found.");
}

echo json_encode($res);
