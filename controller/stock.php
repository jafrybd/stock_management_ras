<?php

include_once('./connection.php');

function test_input($data)
{
	$data = trim($data);
	$data = stripslashes($data);
	return $data;
}

$error_text_color = "red";
$error_point = 0;
$old_message = 0;
$message  = "";
$notification = 0 ;

date_default_timezone_set("Asia/Dhaka");
$nowDateAndTime = date("Y-m-d H-i-s");
// $insertIn

if ($_SERVER["REQUEST_METHOD"] == "POST") {

	if (isset($_POST['updateProductInfo']) || isset($_POST['updateStockValue'])) {

		$id = test_input($_POST["stockId"]);
		if (isset($_POST['updateProductInfo'])) {

			// Selling Price
			if (empty($_POST["sellingPrice"])) {
				// $error_point++;
				$sellingPrice = 0;
			} else {
				$sellingPrice = test_input($_POST["sellingPrice"]);
			}

			// Reorder level
			if (empty($_POST["reorder_Level"])) {
				// $error_point++;
				$reorder_Level = 0;
			} else {
				$reorder_Level = test_input($_POST["reorder_Level"]);
			}

			if (empty($_POST["buingPrice"])) {
				// $error_point++;
				$reorder_Level = 0;
			} else {
				$buingPrice = test_input($_POST["buingPrice"]);
			}

			$quary = "UPDATE `stock` set reorder_level = '" . $reorder_Level . "' , sell_price = '" . $sellingPrice . "' , buing_price = '" . $buingPrice . "' where store_id = " . $store_id . " and id = " . $id;
			$message = ' Stock info Data Update Successfully Done.';
			$notification = 1 ;
		} else {

			// Current Stock description
			if (empty($_POST["currentStock"])) {
				$currentStock = 0;
			} else {
				$currentStock = test_input($_POST["currentStock"]);
			}

			// New Stock description
			if (empty($_POST["newStock"])) {
				$error_point++;
			} else {
				$newStock = test_input($_POST["newStock"]);
			}

			$message = 'Stock info Data Update Successfully Done';
			$methologuy = $_POST["methologuy"];
			$notification = 1 ;

			if ($error_point > 0 || $newStock < 0 || ($methologuy == 'subtract' && ($currentStock - $newStock) < 0)) {
				$message = 'Stock value not negation.';
				$notification = 2 ;
				$error_point++;
				$old_message = 1;
			} else {
				
				$updateStock = ($methologuy == 'add') ? ($currentStock +  $newStock) : ($currentStock - $newStock);
				$quary = "UPDATE `stock` set current_stock = '" . $updateStock . "', last_tran_date = '".$nowDateAndTime."' where store_id = " . $store_id . " and id = " . $id;
			}
		}


		// Check Data

		if ($error_point > 0) {
			$error_point = 0;
			if ($old_message == 0) {
				$message = "Please Fill up all input field";
				$notification = 2 ;
			}
		} else {
			if ($conn->query($quary) === False) {
				$message = 'Fail as : ' . $conn->$quary;
				$notification = 3 ;
			} else if(isset($_POST['updateStockValue'])) {
				
				$tran_type = ($methologuy == 'add') ? 'direct increase': 'direct decrease';
				$productId = test_input($_POST["productId"]);
				$quary = "insert INTO `invoice_tran_item` (tran_type, product_id, qty, store_id) VALUES ('".$tran_type."',".$productId." , ".$newStock.", ". $store_id .")";

				if ($conn->query($quary) === False) {
					$message = 'Fail as Create Record: ' . $conn->$quary;
					$notification = 3 ;
				}
			}
		}
	}
} 
