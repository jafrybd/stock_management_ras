<?php

if (true) {
  header("location: index.php");
} else { ?>



  <!DOCTYPE html>
  <html lang="en">

  <head>
    <?php include_once('partials/header_common.php');  ?>
    <!-- notifications CSS
		============================================ -->
    <link rel="stylesheet" href="public/notification/css/Lobibox.min.css">
    <link rel="stylesheet" href="public/notification/css/notifications.css">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">


            <!-- menu profile quick info -->
            <!-- <% include partials/left_user_prodile.ejs %> -->
            <?php include_once('partials/left_user_prodile.php');  ?>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <!-- <% include partials/left_menu.ejs %> -->
            <?php include_once('partials/left_menu.php');  ?>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <?php include_once('partials/left_buttom_menu.php');  ?>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <!-- <% include partials/right_menu.ejs %> -->
        <?php include_once('partials/right_menu.php');  ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3> <small> </small></h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="notification-list shadow-inner nt-mg-b-30">
                  <div class="alert-title">
                    <h2>Basic notifications</h2>
                    <p>Basic notifications Can be closed by clicking on it.</p>
                  </div>
                  <div class="notification-bt responsive-btn">
                    <button id="basicDefault" class="btn btn-default">Default</button>
                    <button id="basicInfo" class="btn btn-info">Info</button>
                    <button id="basicWarning" class="btn btn-warning">Warning</button>
                    <button id="basicError" class="btn btn-danger">Error</button>
                    <button id="basicSuccess" class="btn btn-success">Success</button>
                    <button onclick="show()"> Show </button>
                  </div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-12  col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Invoice <small> </small></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <section class="content invoice">
                      <!-- title row -->
                      <div class="row">
                        <div class="col-xs-12 invoice-header">
                          <h1>
                            <i class="fa fa-globe"></i> Invoice.
                            <small class="pull-right">Date: 16/08/2016</small>
                          </h1>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- info row -->
                      <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                          From
                          <address> <br>
                            <strong>Monisa Roy</strong>
                            <br>Phone: 01670--145
                          </address>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                          To
                          <address> <br>
                            <strong>John Doe</strong>
                            <br> Phone: 01923-9876
                          </address>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                          <b>Invoice #007612</b>
                          <br>
                          <br>
                          <b>Order ID:</b> 4F3S8J
                          <br>
                          <b>Payment Due:</b> 2/22/2014
                          <br>
                          <b>Account:</b> 968-34567
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->

                      <!-- Table row -->
                      <div class="row" style="padding-top: 30px">
                        <div class="col-xs-12 table">
                          <table class="table table-striped">
                            <thead>
                              <tr>
                                <th>No</th>
                                <th style="width: 15%">Name</th>
                                <th>Lot Number</th>
                                <th>Qty</th>
                                <th>Per Rate</th>
                                <th>Price</th>
                                <th>Discount</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>1</td>
                                <td>Shirt</td>
                                <td>455-981-221</td>
                                <td>10</td>
                                <td>50</td>
                                <td>500</td>
                                <td>50</td>
                              </tr>
                              <tr>
                                <td>2</td>
                                <td>Pant</td>
                                <td>455-981-221</td>
                                <td>10</td>
                                <td>300</td>
                                <td>3000</td>
                                <td>0</td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->

                      <div class="row">
                        <!-- accepted payments column -->
                        <div class="col-xs-6">
                          <p class="lead">Summary:</p>

                          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                            Total Item: 2 <br>
                            Total Qty: 20 product
                          </p>
                        </div>
                        <!-- /.col -->
                        <div class="col-xs-6">
                          <p class="lead">Invoice Payment</p>
                          <div class="table-responsive">
                            <table class="table">
                              <tbody>
                                <tr>
                                  <th style="width:50%">Sub Total:</th>
                                  <td>$250.30</td>
                                </tr>
                                <tr>
                                  <th>Discount </th>
                                  <td>$10.34</td>
                                </tr>
                                <tr>
                                  <th>Net Total:</th>
                                  <td>$265.24</td>
                                </tr>
                                <tr>
                                  <th>Paid</th>
                                  <td>$5.80</td>
                                </tr>
                                <tr>
                                  <th>Due</th>
                                  <td>$5.80</td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->

                      <!-- this row will not appear when printing -->
                      <div class="row no-print">
                        <div class="col-xs-12">
                          <button class="btn btn-default" onclick="window.print();"><i class="fa fa-print"></i> Print</button>
                          <button class="btn btn-primary pull-right" style="margin-right: 5px;"><i class="fa fa-download"></i> Generate PDF</button>
                        </div>
                      </div>
                    </section>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Power By Rename Tech
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="public/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="public/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="public/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="public/vendors/nprogress/nprogress.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="public/build/js/custom.min.js"></script>

    <!-- notification JS
		============================================ -->
    <script src="public/notification/js/Lobibox.js"></script>
    <script src="public/notification/js/notification-active.js"></script>

    <script>
      function show() {
        Lobibox.notify('success', {
          sound: false,
          position: 'top right',
          msg: 'Lorem ipsum dolor sit amet against apennine any created, spend loveliest, building stripes.'
        });
      }

      (function() {
        show()
      })()
    </script>
  </body>

  </html>

<?php  } ?>