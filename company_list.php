<?php

session_start();


if (isset($_SESSION['store_management_user_id'])) {
  $store_id = $_SESSION['store_management_store_id'];
  include_once('controller/company.php');

  $quary = "SELECT * FROM `company` where store_id = " . $store_id;
  $resultDataList = mysqli_query($conn, $quary);
} else {
  header("location: 1index.php");
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <?php include_once('partials/header_common.php');  ?>

  <!-- iCheck -->
  <link href="public/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
  <!-- Datatables -->
  <link href="public/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
  <link href="public/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
  <link href="public/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
  <link href="public/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
  <link href="public/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

  <!-- bootstrap-daterangepicker -->
  <link href="public/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
  <!-- bootstrap-datetimepicker -->
  <link href="public/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">


</head>

<body class="nav-md">
  <div class="container body">
    <div class="main_container">
      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">

          <!-- menu profile quick info -->
          <?php include_once('partials/left_user_prodile.php');  ?>
          <!-- /menu profile quick info -->

          <br />

          <!-- sidebar menu -->
          <?php include_once('partials/left_menu.php');  ?>
          <!-- /sidebar menu -->

          <!-- /menu footer buttons -->
          <?php include_once('partials/left_buttom_menu.php');  ?>
          <!-- /menu footer buttons -->

        </div>
      </div>

      <!-- top navigation -->
      <?php include_once('partials/right_menu.php');  ?>
      <!-- /top navigation -->

      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>Company List<small></small></h3>
            </div>


            <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  <button type="button" class="btn btn-round btn-success" data-toggle="modal" data-target="#Modal_add_new_company">Add New Company</button>
                </div>
              </div>
            </div>
          </div>

          <div class="clearfix"></div>

          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <!-- <h3><%= message %></h3> -->
              <div class="x_panel">
                <div class="x_content">

                  <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Company Name</th>
                        <th>Description</th>
                        <th>Creation Date</th>
                        <th>visibility</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      $i = 1;
                      while ($row = mysqli_fetch_array($resultDataList)) {   ?>
                        <tr>
                          <td><?php echo $i++ ?></td>
                          <td><?php echo $row['company_name'] ?></td>
                          <td><?php echo $row['company_description'] ?></td>
                          <td><?php echo $row['creation_date'] ?></td>
                          <td><?php if ($row['visibility']) echo "True";
                              else echo "False" ?></td>
                          <td>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#Modal_edit" onclick="changeEditProductInfo('<?php echo $row['id'] ?>', '<?php echo $row['company_name'] ?>', '<?php echo $row['company_description'] ?>','<?php echo $row['visibility'];  ?>')">Edit</button>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#Modal_delete" onclick="changeDeleteProductInfo('<?php echo $row['id'] ?>', '<?php echo $row['company_name'] ?>')">Delete</button>
                          </td>
                        </tr>
                      <?php   }  ?>
                    </tbody>
                  </table>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /page content -->

      <!--  mode start Delete -->

      <div class="modal fade bs-example-modal-sm" id="Modal_delete" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm">
          <div class="modal-content">

            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
              </button>
              <h4 class="modal-title" id="myModalLabel2">Warning</h4>
            </div>
            <div class="modal-body">
              <h4 id="deleteProductMessage">Will </h4>
            </div>
            <form id="demo-form2" method="POST" action="<?php echo $_SERVER["PHP_SELF"]; ?>" enctype="multipart/form-data" role="form" data-parsley-validate class="form-horizontal form-label-left">
              <div class="modal-footer">
                <input id="deleteProductId" name="deleteProductId" type="hidden" value="0">
                <input type="submit" name="autherDelete" class="btn btn-danger" value="Yes">
                <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                <!-- <button type="submit" class="btn btn-success">Submit</button> -->
              </div>
            </form>
          </div>
        </div>
      </div>

      <!-- modal end  -->


      
      <!--  mode start Edit  -->

      <div class="modal fade bs-example-modal-sm" id="Modal_edit" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm">
          <div class="modal-content">

            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
              </button>
              <h4 class="modal-title" id="myModalLabel2">Edit Company</h4>
            </div>
            <form id="demo-form2" method="POST" action="<?php echo $_SERVER["PHP_SELF"]; ?>" enctype="multipart/form-data" role="form" data-parsley-validate class="form-horizontal form-label-left">

              <div class="modal-body">
                <div class="form-group">
                  <label class="col-md-12 col-sm-12 col-xs-12" for="first-name">Product
                    Name <span class="required">*</span>
                  </label>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <input type="text" id="productName" required="required" name="form_company_name" class="form-control col-md-7 col-xs-12">
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-md-12 col-sm-12 col-xs-12" for="productDescription"> Description <span class="required">*</span>
                  </label>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <textarea rows="4" id="productDescription" class="form-control col-md-7 col-xs-12" type="text" name="form_company_description"></textarea>
                  </div>
                </div>
                <input type="hidden" id="categoryId" name="companyId">


                <div class="form-group">
                  <label class="col-md-12 col-sm-12 col-xs-12" for="editProductvisibility"> Visibility <span class="required">*</span>
                  </label>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <select id="editProductvisibility" name="form_visibility" class="form-control" required>
                      <option value="True">True</option>
                      <option value="False">False</option>
                    </select>
                  </div>
                </div>

              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" name="ProductDetailsUpdate" class="btn btn-primary">Save changes</button>
              </div>
            </form>
          </div>
        </div>
      </div>

      <!-- modal end  -->



      <!--  mode start Add New Company  -->

      <div class="modal fade bs-example-modal-sm" id="Modal_add_new_company" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm">
          <div class="modal-content">

            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
              </button>
              <h4 class="modal-title" id="myModalLabel2">Add New Company</h4>
            </div>
            <form id="demo-form2" method="POST" action="<?php echo $_SERVER["PHP_SELF"]; ?>" enctype="multipart/form-data" role="form" data-parsley-validate class="form-horizontal form-label-left">

              <div class="modal-body">
                <div class="form-group">
                  <label class="col-md-12 col-sm-12 col-xs-12" for="form_company_name">Product
                    Name <span class="required">*</span>
                  </label>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <input type="text" id="form_company_name" required="required" name="form_company_name" class="form-control col-md-7 col-xs-12">
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-md-12 col-sm-12 col-xs-12" for="form_company_description"> Description <span class="required">*</span>
                  </label>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <textarea rows="4" id="form_company_description" class="form-control col-md-7 col-xs-12" type="text" name="form_company_description"></textarea>
                  </div>
                </div>

                <div class="form-group clearfix">
                  <label class="col-md-12 col-sm-12 col-xs-12" for="productVisibility"> Visibility <span class="required">*</span>
                  </label>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <select id="form_visibility" name="form_visibility" class="form-control" required>
                      <option value="True">True</option>
                      <option value="False">False</option>
                    </select> </div>
                </div>



                <input type="hidden" id="productId" name="productId">
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" name="addCompany" class="btn btn-primary">Add</button>
              </div>
            </form>
          </div>
        </div>
      </div>

      <!-- modal end  Add New Company -->



      <!-- footer content -->
      <?php include_once('partials/footer.php');  ?>
      <!-- /footer content -->
    </div>
  </div>

  <!-- jQuery -->
  <script src="public/vendors/jquery/dist/jquery.min.js"></script>
  <!-- Bootstrap -->
  <script src="public/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- FastClick -->
  <script src="public/vendors/fastclick/lib/fastclick.js"></script>
  <!-- NProgress -->
  <script src="public/vendors/nprogress/nprogress.js"></script>
  <!-- iCheck -->
  <script src="public/vendors/iCheck/icheck.min.js"></script>
  <!-- Datatables -->
  <script src="public/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="public/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
  <script src="public/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
  <script src="public/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
  <script src="public/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
  <script src="public/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
  <script src="public/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
  <script src="public/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
  <script src="public/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
  <script src="public/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
  <script src="public/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
  <script src="public/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
  <script src="public/vendors/jszip/dist/jszip.min.js"></script>
  <script src="public/vendors/pdfmake/build/pdfmake.min.js"></script>
  <script src="public/vendors/pdfmake/build/vfs_fonts.js"></script>


  <!-- Custom Theme Scripts -->
  <script src="public/build/js/custom.min.js"></script>


  <script>
    function changeDeleteProductInfo(id, name) {
      document.getElementById("deleteProductMessage").innerHTML = "Will You Really Delete <span style='color:blue'>" + name + "</span> Company";
      document.getElementById("deleteProductId").value = id;
    }

    function changeEditProductInfo(id, name, description, visibility) {
      document.getElementById("productName").value = name;
      document.getElementById("productDescription").value = description;
      document.getElementById("categoryId").value = id;
      document.getElementById("editProductvisibility").value = (visibility == 1) ? "True" : "False";
    }
  </script>

  <?php include_once('partials/notification.php');  ?>
</body>

</html>