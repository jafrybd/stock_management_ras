<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
  <div class="menu_section">
    <h3>General</h3>
    <ul class="nav side-menu">
      <li><a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="1index.php">Dashboard</a></li>
        </ul>
      </li>
      <li><a><i class="fa fa-edit"></i> Invoice <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
        <li><a href=" add_sells_invoice.php">Sells</a></li>
          <li><a href="add_invoice.php">Add Invoice</a></li>
          <li><a href="invoice_list.php">Invoice List</a></li>
          <!-- <li><a href="1index.php">Invoice Details</a></li> -->
        </ul>
      </li>
      <li><a><i class="fa fa-edit"></i> Product Materials<span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="product-head-list.php">Product Head List</a></li>
          <li><a href="category-list.php">Product Category List</a></li>
          <li><a href="product-type-list.php">Product Type List</a></li>
          <li><a href="group-list.php">Product Group List</a></li>
          <li><a href="product-size-list.php">Size List</a></li>
        </ul>
      </li>
      <li><a><i class="fa fa-edit"></i> Product <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="add_new_product.php">Add Product</a></li>
          <li><a href="product_list.php">Product List</a></li>
        </ul>
      </li>

      <li><a><i class="fa fa-edit"></i> Category <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="add_new_category.php">Add Category</a></li>
          <li><a href="category_list.php">Category List</a></li>
        </ul>
      </li>

      <li><a><i class="fa fa-edit"></i> Company <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <!-- <li><a href="add_new_company.php">Add Company</a></li> -->
          <li><a href="company_list.php">Company List</a></li>
        </ul>
      </li>
      <!-- <li><a><i class="fa fa-edit"></i> User <span class="fa fa-chevron-down"></span></a>
          <ul class="nav child_menu">
            <li><a href="/add_user">Add User</a></li>
            <li><a href="/user_list">User List</a></li>
          </ul>
        </li> -->
      <li><a><i class="fa fa-edit"></i> Stock <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="stock_list.php">Current Stock</a></li>
          <li><a href="reorder_level_stock_list.php">Reorder Level Product</a></li>
        </ul>
      </li>
      <li><a><i class="fa fa-edit"></i> Generic <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="generic_list.php">Generic List</a></li>
        </ul>
      </li>
      <li><a><i class="fa fa-edit"></i> Report <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="day_by_product_report.php">Day By Report</a></li>
        </ul>
      </li>

      

    </ul>
  </div>


</div>