<div class="navbar nav_title" style="border: 0;">
    <a href="#" class="site_title"><i class="fa fa-paw"></i> <span><?php echo $_SESSION['store_management_store_name']; ?></span></a>
  </div>

  <div class="clearfix"></div>

<div class="profile clearfix">
    <div class="profile_pic">
      <img src="public/images/img.jpg" alt="..." class="img-circle profile_img">
    </div>
    <div class="profile_info">
      <span>Welcome </span>
      <h2><?php echo $_SESSION['store_management_user_name']; ?></h2>
    </div>
  </div>