<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Stock Management </title>

<!-- Bootstrap -->
<link href="public/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="public/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="public/vendors/nprogress/nprogress.css" rel="stylesheet">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<!-- Custom styling plus plugins -->
<link href="public/build/css/custom.min.css" rel="stylesheet">

<!-- notifications CSS ======= -->
<link rel="stylesheet" href="public/notification/css/Lobibox.min.css">
<link rel="stylesheet" href="public/notification/css/notifications.css">