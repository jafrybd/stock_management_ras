<!-- notification JS =================================== -->
<script src="public/notification/js/Lobibox.js"></script>

<script>
    function show() {
        var a = <?php echo $notification; ?>;
        

        try {
            var message1 = <?php echo json_encode($message); ?>;
        } catch (e) {
            var message1 = "";
        }
        
        
        if (a == 1) {
            Lobibox.notify('success', {
                position: 'bottom right',
                msg: message1
            });
        } else if(a == 2) {   // 
            Lobibox.notify('warning', {
                    position: 'bottom right',
                    msg: message1,
                });
        } else if(a == 3){
            Lobibox.notify('error', {
                    position: 'bottom right',
                    msg: message1
                });
        }

    }

    (function() {
        show();
    })()
</script>