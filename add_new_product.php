<?php

session_start();


if (isset($_SESSION['store_management_user_id'])) {
    $store_id = $_SESSION['store_management_store_id'];
    include_once('controller/product.php');

    $quary = "SELECT * FROM `category` where sub_cat_id = 0 and store_id = " . $store_id;
    $resultCatList = mysqli_query($conn, $quary);

    $quary = "SELECT * FROM `generic` where store_id = " . $store_id;
    $resultGenericList = mysqli_query($conn, $quary);

    $quary = "SELECT * FROM `company` where store_id = " . $store_id;
    $resultCompanyList = mysqli_query($conn, $quary);
} else {
    header("location: 1index.php");
}

?>


<!DOCTYPE html>
<html lang="en">

<head>
    <?php include_once('partials/header_common.php');  ?>

    <!-- iCheck -->
    <link href="public/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="public/vendors/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
    <!-- Select2 -->
    <link href="public/vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <!-- Switchery -->
    <link href="public/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
    <!-- starrr -->
    <link href="public/vendors/starrr/dist/starrr.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="public/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>

    <script>
        function getSubCat(val) {
            // alert("i was call");
            $.ajax({
                type: "POST",
                url: "controller/product_subCat_dropdown.php",
                data: 'countryid=' + val,
                success: function(data) {
                    $("#product_sub_category").html(data);
                }
            });

            // changeGeneric();
        }
    </script>
</head>

<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">

                    <!-- menu profile quick info -->
                    <?php include_once('partials/left_user_prodile.php');  ?>
                    <!-- /menu profile quick info -->

                    <br />

                    <!-- sidebar menu -->
                    <?php include_once('partials/left_menu.php');  ?>
                    <!-- /sidebar menu -->

                    <!-- /menu footer buttons -->
                    <?php include_once('partials/left_buttom_menu.php');  ?>
                    <!-- /menu footer buttons -->
                </div>
            </div>

            <!-- top navigation -->
            <?php include_once('partials/right_menu.php');  ?>
            <!-- /top navigation -->

            <!-- page content -->
            <div class="right_col" role="main">
                <div class="">
                    <div class="page-title">
                        <div class="title_left">
                            <h3>Add New Product</h3>
                        </div>

                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button">Go!</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Product <small></small></h2>

                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br />
                                    <form id="demo-form2" method="POST" action="<?php echo $_SERVER["PHP_SELF"]; ?>" enctype="multipart/form-data" role="form" data-parsley-validate class="form-horizontal form-label-left">
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                <?php //echo $message; 
                                                ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="product_name">Product
                                                Product Name <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="product_name" required="required" name="product_name" class="form-control col-md-7 col-xs-12">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="product_category">Product
                                                Product Category <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select id="product_category" name="product_category" class="form-control" onChange="getSubCat(this.value);" required>
                                                    <option value="-10"> Select a Category</option>
                                                    <?php while ($row = mysqli_fetch_array($resultCatList)) {   ?>
                                                        <option value="<?php echo $row['id']; ?>"> <?php echo $row["cat_name"]; ?></option>
                                                    <?php   }  ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="product_sub_category">Product
                                                Product Sub Category <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select id="product_sub_category" name="product_sub_category" class="form-control" required>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group" id="generic_div">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="product_generic">
                                                Generic <span class="required">*</span>
                                            </label>

                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select id="product_generic" name="product_generic" class="form-control" required>
                                                    <!-- <option value="-10"> Select a Category</option> -->
                                                    <?php while ($row = mysqli_fetch_array($resultGenericList)) {   ?>
                                                        <option value="<?php echo $row['generic_name']; ?>"> <?php echo $row["generic_name"]; ?></option>
                                                    <?php   }  ?>
                                                </select>
                                            </div>
                                        </div>



                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="product_generic">
                                                Company Name <span class="required">*</span>
                                            </label>

                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select id="product_company" name="product_company" class="form-control" required>
                                                    <?php while ($row = mysqli_fetch_array($resultCompanyList)) {   ?>
                                                        <option value="<?php echo $row['id']; ?>"> <?php echo $row["company_name"]; ?></option>
                                                    <?php   }  ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="qty_in_box">Product
                                                Qty In Box <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="number" min="1" value="1" id="qty_in_box" required="required" name="qty_in_box" class="form-control col-md-7 col-xs-12">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="buying_price_in_box">Product
                                                Buy Prince (In Box) <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="number" min="1" value="1" id="buying_price_in_box" required="required" name="buying_price_in_box" class="form-control col-md-7 col-xs-12">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="saleing_price_in_box">Product
                                                Sell Prince (In Box) <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="number" min="1" value="1" id="saleing_price_in_box" required="required" name="saleing_price_in_box" class="form-control col-md-7 col-xs-12">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="form_visibility">Product
                                                Visibility <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="checkbox" name="form_visibility" class="js-switch" checked />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="discription" class="control-label col-md-3 col-sm-3 col-xs-12">Discription</label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea rows="4" id="discription" class="form-control col-md-7 col-xs-12" type="text" name="discription"></textarea>
                                            </div>
                                        </div>
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                <button class="btn btn-primary" type="reset">Reset</button>
                                                <button type="submit" name="AddProduct" class="btn btn-success">Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>


                    <!-- footer content -->
                    <?php include_once('partials/footer.php');  ?>
                    <!-- /footer content -->
                </div>
            </div>

            <!-- jQuery -->
            <script src="public/vendors/jquery/dist/jquery.min.js"></script>
            <!-- Bootstrap -->
            <script src="public/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
            <!-- FastClick -->
            <script src="public/vendors/fastclick/lib/fastclick.js"></script>
            <!-- NProgress -->
            <script src="public/vendors/nprogress/nprogress.js"></script>
            <!-- bootstrap-progressbar -->
            <script src="public/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
            <!-- iCheck -->
            <script src="public/vendors/iCheck/icheck.min.js"></script>
            <!-- bootstrap-daterangepicker -->
            <script src="public/vendors/moment/min/moment.min.js"></script>
            <script src="public/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
            <!-- bootstrap-wysiwyg -->
            <script src="public/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
            <script src="public/vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
            <script src="public/vendors/google-code-prettify/src/prettify.js"></script>
            <!-- jQuery Tags Input -->
            <script src="public/vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
            <!-- Switchery -->
            <script src="public/vendors/switchery/dist/switchery.min.js"></script>
            <!-- Select2 -->
            <script src="public/vendors/select2/dist/js/select2.full.min.js"></script>
            <!-- Parsley -->
            <script src="public/vendors/parsleyjs/dist/parsley.min.js"></script>
            <!-- Autosize -->
            <script src="public/vendors/autosize/dist/autosize.min.js"></script>
            <!-- jQuery autocomplete -->
            <script src="public/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
            <!-- starrr -->
            <script src="public/vendors/starrr/dist/starrr.js"></script>
            <!-- Custom Theme Scripts -->
            <script src="public/build/js/custom.min.js"></script>

            <script>
                function changeGeneric(){
                    let e = document.getElementById("product_category");
                    let text = e.options[e.selectedIndex].text;
                   if (text === 'Medicine'){
                    document.getElementById("generic_div").style.visibility = 'visible';
                   } else {
                    document.getElementById("generic_div").style.visibility = 'hidden';
                   }
                }
            </script>

            <?php include_once('partials/notification.php');  ?>


</body>

</html>