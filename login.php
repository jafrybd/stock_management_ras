<?php
// define variables and set to empty values
session_start();

function test_input($data)
{
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

if (isset($_SESSION['store_management_user_id'])) {
  header("location: 1index.php");  // if login, redirect to dashboard
} else {


  $email = $password = "";
  $id = 0;

  require_once('connection.php');

  if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $user_phone = test_input($_POST["user_phone"]);
    $password = test_input($_POST["user_password"]);

    $user_phone = strlen($user_phone) < 1 ? "0909" : $user_phone;
    $password = strlen($password) < 1 ? "0909" : $password;

    $sql = "SELECT * FROM user  where user_id = '$user_phone' ";
    $result = mysqli_query($conn, $sql);
    $store_id = 0;


    if (mysqli_num_rows($result) == 1) {

      while ($row = mysqli_fetch_array($result)) {
        $id = $row['user_id'];
        $id_no = $row['id'];
        $name = $row['user_name'];
        $phone_number = $row['phone_no'];
        $password_hash = $row['password'];
        $email = $row['email'];
        $store_id = $row['store_id'];
        break;
      }

      if (password_verify($password, $password_hash)) {
      

        $sql = "SELECT * FROM `store` where id = " . $store_id;
        $result = mysqli_query($conn, $sql);

        while ($row = mysqli_fetch_array($result)) {
          $store_name = $row['store_name'];
          $store_address = $row['store_address'];
          break;
        }

        $_SESSION['store_management_user_id'] = $id;
        $_SESSION['store_management_user_name'] = $name;
        $_SESSION['store_management_user_id_no'] = $id_no;
        $_SESSION['store_management_store_id'] = $store_id;
        $_SESSION['stockManagement_SelectedProductList'] = array();
        $_SESSION['store_management_store_name'] = $store_name;
        $_SESSION['store_management_store_address'] = $store_address;
        header("Location: 1index.php");

      } {
        header("Location: login.php?error=true");
      }
    } else {
      header("Location: login.php?error=true");
    }
  }


  if ($_GET) {

    if (isset($_GET['error']) && $_GET['error'] == true) {
      $message = "Wrong password or phone or both <br> <br> <br>";
    }
  } else {
    $message = "";
  }

?>




  <!DOCTYPE html>
  <html lang="en">

  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Gentelella Alela! | </title>

    <!-- Bootstrap -->
    <link href="public/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="public/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="public/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="public/vendors/animate.css/animate.min.css" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Custom Theme Style -->
    <link href="public/build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
              <h1>Login</h1>
              <div>
                <input type="text" class="form-control" placeholder="Userid / phone" required="" name="user_phone" />
              </div>
              <div>
                <input type="password" class="form-control" placeholder="Password" name="user_password" required="" />
              </div>
              <div>
                <?php echo  $message;  ?>
                <!-- <a class="btn btn-default submit" type="submit">Log in</a> -->
                <input type="submit" value="Login" class="btn btn-default submit">
                <a class="reset_pass" href="#">Lost your password?</a>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <!-- <p class="change_link">New to site?
                  <a href="#signup" class="to_register"> Create Account </a>
                </p> -->


                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><i class="fa fa-paw"></i> Stock Management System</h1>
                  <p>©<?php echo date("Y"); ?> All Rights Reserved Rename Tech</p>
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>
  </body>

  </html>

<?php } ?>