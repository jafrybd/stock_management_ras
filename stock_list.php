<?php

session_start();


if (isset($_SESSION['store_management_user_id'])) {
  $store_id = $_SESSION['store_management_store_id'];
  include_once('controller/stock.php');
  $quary = "SELECT * FROM `stock` WHERE store_id = " . $store_id;
  $resultDataList = mysqli_query($conn, $quary);
} else {
  header("location: login.php");
}

?>






<!DOCTYPE html>
<html lang="en">

<head>
  <?php include_once('partials/header_common.php');  ?>

  <!-- iCheck -->
  <link href="public/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
  <!-- Datatables -->
  <link href="public/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
  <link href="public/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
  <link href="public/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
  <link href="public/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
  <link href="public/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

  <!-- bootstrap-daterangepicker -->
  <link href="public/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
  <!-- bootstrap-datetimepicker -->
  <link href="public/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">

</head>

<body class="nav-md">
  <div class="container body">
    <div class="main_container">
      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">

          <!-- menu profile quick info -->
          <?php include_once('partials/left_user_prodile.php');  ?>
          <!-- /menu profile quick info -->

          <br />

          <!-- sidebar menu -->
          <?php include_once('partials/left_menu.php');  ?>
          <!-- /sidebar menu -->

          <!-- /menu footer buttons -->
          <?php include_once('partials/left_buttom_menu.php');  ?>
          <!-- /menu footer buttons -->

        </div>
      </div>

      <!-- top navigation -->
      <?php include_once('partials/right_menu.php');  ?>
      <!-- /top navigation -->

      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>Stock<small></small></h3>
            </div>

            <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="Search for...">
                  <span class="input-group-btn">
                    <button class="btn btn-default" type="button">Go!</button>
                  </span>
                </div>
              </div>
            </div>
          </div>

          <div class="clearfix"></div>

          <div class="row">

            <?php //echo $message; 
            ?>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_content">
                  <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Product Name</th>
                        <th>Current Stock</th>
                        <th>Buing Price (pcs)</th>
                        <th>Selling Price (pcs)</th>
                        <th>Average Price (pcs)</th>
                        <th>Last Tran Date</th>
                        <th>Reorder Level</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      $i = 1;
                      while ($row = mysqli_fetch_array($resultDataList)) {   ?>
                        <tr>
                          <td><?php echo $i++ ?></td>
                          <td><?php echo $row['product_name'] ?></td>
                          <td><?php echo $row['current_stock'] ?></td>
                          <td><?php echo $row['buing_price'] ?></td>
                          <td><?php echo $row['sell_price'] ?></td>
                          <td><?php echo $row['average_price'] ?></td>
                          <td><?php echo $row['last_tran_date'] ?></td>
                          <td><?php echo $row['reorder_level'] ?></td>
                          <td>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#Modal_edit" onclick="changeEditProductInfo('<?php echo $row['id'] ?>', '<?php echo $row['product_name']; ?>', '<?php echo $row['sell_price']; ?>', '<?php echo $row['reorder_level']; ?>', '<?php echo $row['buing_price']; ?>')">Edit</button>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#Modal_update_stock" onclick="UpdateProductStock('<?php echo $row['id'] ?>', '<?php echo $row['product_name']; ?>', '<?php echo $row['current_stock']; ?>', '<?php echo $row['product_id']; ?>',)">Update Stock</button>
                          </td>
                        </tr>
                      <?php   }  ?>
                    </tbody>

                  </table>


                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /page content -->

      <!--  mode start Edit  -->

      <div class="modal fade bs-example-modal-sm" id="Modal_edit" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm">
          <div class="modal-content">

            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
              </button>
              <h4 class="modal-title" id="myModalLabel2">Edit Product Stock</h4>
            </div>
            <form id="demo-form2" method="POST" action="<?php echo $_SERVER["PHP_SELF"]; ?>" enctype="multipart/form-data" role="form" data-parsley-validate class="form-horizontal form-label-left">

              <div class="modal-body">
                <div class="form-group">
                  <label class="col-md-12 col-sm-12 col-xs-12" for="productName">Product
                    Name <span class="required"></span>
                  </label>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <input type="text" id="productName" required="required" name="productName" class="form-control col-md-7 col-xs-12" readonly>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-md-12 col-sm-12 col-xs-12" for="buingPrice">Product
                    Buing Price <span class="required">*</span>
                  </label>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <input type="number" id="buingPrice" min="1" step="any" required="required" name="buingPrice" class="form-control col-md-7 col-xs-12">
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-md-12 col-sm-12 col-xs-12" for="sellingPrice">Product
                    Selling Price <span class="required">*</span>
                  </label>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <input type="number" id="sellingPrice" min="1" step="any" required="required" name="sellingPrice" class="form-control col-md-7 col-xs-12">
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-md-12 col-sm-12 col-xs-12" for="reorder_Level">Product
                    Reorder Level <span class="required">*</span>
                  </label>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <input type="number" step="any" id="reorder_Level" required="required" name="reorder_Level" class="form-control col-md-7 col-xs-12">
                  </div>
                </div>

                <input type="hidden" id="stockId" name="stockId">
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" name="updateProductInfo" class="btn btn-primary">Save changes</button>
              </div>
            </form>
          </div>
        </div>
      </div>

      <!-- modal end  -->



      <!--  mode start Update Stock  -->

      <div class="modal fade bs-example-modal-sm" id="Modal_update_stock" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm">
          <div class="modal-content">

            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
              </button>
              <h4 class="modal-title" id="myModalLabel2">Update Product Stock</h4>
            </div>
            <form id="demo-form2" method="POST" action="<?php echo $_SERVER["PHP_SELF"]; ?>" enctype="multipart/form-data" role="form" data-parsley-validate class="form-horizontal form-label-left">

              <div class="modal-body">
                <div class="form-group">
                  <label class="col-md-12 col-sm-12 col-xs-12" for="first-name">Product
                    Name <span class="required"></span>
                  </label>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <input type="text" id="productName1" name="productName1" class="form-control col-md-7 col-xs-12" readonly>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-md-12 col-sm-12 col-xs-12" for="currentStock">Product
                    Current Stock <span class="required">*</span>
                  </label>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <input type="number" min="-5" id="currentStock" required="required" name="currentStock" class="form-control col-md-7 col-xs-12" readonly>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-md-12 col-sm-12 col-xs-12" for="newStock">Product
                    New Stock <span class="required">*</span>
                  </label>
                  <div class="col-md-8 col-sm-8 col-xs-8">
                    <input type="number" min="1" id="newStock" required="required" name="newStock" class="form-control col-md-7 col-xs-12">
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-4">
                    <select id="methologuy" name="methologuy" class="form-control" required>
                      <option value="add">+</option>
                      <option value="subtract">-</option>
                    </select>
                  </div>
                </div>

                <input type="hidden" id="stockId1" name="stockId">
                <input type="hidden" id="productId" name="productId">
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" name="updateStockValue" class="btn btn-primary">Save changes</button>
              </div>
            </form>
          </div>
        </div>
      </div>

      <!-- modal end  -->



      <!-- footer content -->
      <?php include_once('partials/footer.php');  ?>
      <!-- /footer content -->
    </div>
  </div>

  <!-- jQuery -->
  <script src="public/vendors/jquery/dist/jquery.min.js"></script>
  <!-- Bootstrap -->
  <script src="public/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- FastClick -->
  <script src="public/vendors/fastclick/lib/fastclick.js"></script>
  <!-- NProgress -->
  <script src="public/vendors/nprogress/nprogress.js"></script>
  <!-- iCheck -->
  <script src="public/vendors/iCheck/icheck.min.js"></script>
  <!-- Datatables -->
  <script src="public/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="public/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
  <script src="public/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
  <script src="public/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
  <script src="public/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
  <script src="public/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
  <script src="public/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
  <script src="public/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
  <script src="public/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
  <script src="public/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
  <script src="public/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
  <script src="public/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
  <script src="public/vendors/jszip/dist/jszip.min.js"></script>
  <script src="public/vendors/pdfmake/build/pdfmake.min.js"></script>
  <script src="public/vendors/pdfmake/build/vfs_fonts.js"></script>


  <!-- Custom Theme Scripts -->
  <script src="public/build/js/custom.min.js"></script>


  <script>
    function changeEditProductInfo(id, name, sell_price, reorder_Level, buing_price) {
      document.getElementById("productName").value = name;
      document.getElementById("sellingPrice").value = sell_price;
      document.getElementById("stockId").value = id;
      document.getElementById("reorder_Level").value = reorder_Level;
      document.getElementById("buingPrice").value = buing_price;
    }

    function UpdateProductStock(id, name, current_stock, product_id) {
      document.getElementById("productName1").value = name;
      document.getElementById("currentStock").value = current_stock;
      document.getElementById("stockId1").value = id;
      document.getElementById("productId").value = product_id;
    }
  </script>

  <?php include_once('partials/notification.php');  ?>

</body>

</html>