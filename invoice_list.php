<?php

session_start();


if (isset($_SESSION['store_management_user_id'])) {
    $store_id = $_SESSION['store_management_store_id'];
    include_once('controller/addInvoiceToCard.php');

    date_default_timezone_set("Asia/Dhaka");
    $fromData = $toDate = date("Y-m-d");
    // $tran_type = "all";
    $quary = "SELECT * FROM `invoice_tran` WHERE store_id = " . $store_id . " order by id DESC";

    // This Code For Search
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $extraWhere = " ";

        if ($_POST['type'] != "all") {
            $extraWhere = " tran_type = '" . $_POST['type'] . "'  and";
        }

        if (empty($_POST['fromDate']) || empty($_POST['toDate'])) {
            $quary = "SELECT * FROM `invoice_tran` WHERE " . $extraWhere . " store_id = " . $store_id . " order by id DESC";
        } else {
            $fromData = $_POST['fromDate'];
            $toDate = $_POST['toDate'];
            $quary = "SELECT * FROM `invoice_tran` WHERE " . $extraWhere . " creating_date >= '" . $_POST['fromDate'] . "' and creating_date <= '" . date('Y-m-d', strtotime($toDate . ' + 1 days')) . "' and store_id = " . $store_id . " order by id DESC";
        }
    }

    $resultDataList = mysqli_query($conn, $quary);
} else {
    header("location: login.php");
}

?>


<!DOCTYPE html>
<html lang="en">

<head>
    <?php include_once('partials/header_common.php');  ?>
    <!-- Bootstrap -->

    <!-- iCheck -->
    <link href="public/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="public/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="public/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="public/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="public/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="public/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

    <!-- bootstrap-daterangepicker -->
    <link href="public/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <!-- bootstrap-datetimepicker -->
    <link href="public/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
</head>

<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">

                    <!-- menu profile quick info -->
                    <?php include_once('partials/left_user_prodile.php');  ?>
                    <!-- /menu profile quick info -->

                    <br />

                    <!-- sidebar menu -->
                    <?php include_once('partials/left_menu.php');  ?>
                    <!-- /sidebar menu -->

                    <!-- /menu footer buttons -->
                    <?php include_once('partials/left_buttom_menu.php');  ?>
                    <!-- /menu footer buttons -->

                </div>
            </div>

            <!-- top navigation -->
            <?php include_once('partials/right_menu.php');  ?>
            <!-- /top navigation -->

            <!-- page content -->
            <div class="right_col" role="main">
                <div class="">
                    <div class="page-title">
                        <div class="title_left">
                            <h3>Invoice List<small></small></h3>
                        </div>

                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button">Go!</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="row">


                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="well" style="overflow: auto">
                                    <form id="demo-form2" method="POST" action="<?php echo $_SERVER["PHP_SELF"]; ?>" enctype="multipart/form-data" role="form" data-parsley-validate class="form-horizontal form-label-left">
                                        <div class="col-md-3">
                                            Invoice Type:
                                            <div class="form-group">
                                                <select class="select2_single form-control" id="tran_type" name="type" tabindex="-1">
                                                    <option value="all">All</option>
                                                    <option value="Receive From Supplier">Receive From Supplier</option>
                                                    <option value="Return to Supplier">Return to Supplier</option>
                                                    <option value="Sell To Customer">Sell To Customer</option>
                                                    <option value="Customer Return">Customer Return</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div style="padding-left: 40px; padding-right: 40px">
                                                From Date:
                                                <div class="input-prepend input-group">
                                                    <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                    <input id="date" name="fromDate" value="<?php echo $fromData; ?>" style="width: 200px" type="date" value="t">
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-md-3">
                                            To Date:
                                            <div class="input-prepend input-group">
                                                <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                <input id="date" name="toDate" value="<?php echo $toDate; ?>" style="width: 200px" type="date" value="t">
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div style="padding-top: 15px">
                                                <input type="submit" value="Search" class="btn btn-round btn-success btn-sm">
                                            </div>
                                        </div>

                                        <form>

                                            <div class="clearfix"></div>
                                </div>
                                <div class="x_content">

                                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Poi Name</th>
                                                <th>Invoice type</th>
                                                <th>Date</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $i = 1;
                                            while ($row = mysqli_fetch_array($resultDataList)) {
                                            ?>
                                                <tr>
                                                    <td><?php echo $i++ ?></td>
                                                    <td><?php echo $row['customer_name'] ?></td>
                                                    <td><?php echo $row['tran_type'] ?></td>
                                                    <td><?php echo $row['creating_date'] ?></td>
                                                    <td><a href="invoice_details.php?id=<?php echo $row['id']; ?>"><button type="button" class="btn btn-primary">View</button></a> </td>
                                                </tr>
                                            <?php   }  ?>
                                        </tbody>
                                    </table>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /page content -->

            <!-- footer content -->
            <?php include_once('partials/footer.php');  ?>
            <!-- /footer content -->
        </div>
    </div>

    <!-- jQuery -->
    <script src="public/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="public/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="public/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="public/vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="public/vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="public/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="public/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="public/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="public/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="public/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="public/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="public/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="public/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="public/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="public/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="public/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="public/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="public/vendors/jszip/dist/jszip.min.js"></script>
    <script src="public/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="public/vendors/pdfmake/build/vfs_fonts.js"></script>


    <!-- Custom Theme Scripts -->
    <script src="public/build/js/custom.min.js"></script>

    <?php // include_once('partials/notification.php');  
    ?>

</body>

</html>