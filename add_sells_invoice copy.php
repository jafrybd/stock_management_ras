<?php

session_start();

date_default_timezone_set("Asia/Dhaka");
$time = time();
$date = date("d M, Y", $time);

if (isset($_SESSION['store_management_user_id'])) {
  $store_id = $_SESSION['store_management_store_id'];
  $store_name = $_SESSION['store_management_store_name'];
  $invoiceCreatName = $_SESSION['store_management_user_name'];
  $invoiceCreatorId = $_SESSION['store_management_user_id_no'];
  include_once('controller/addInvoiceToCard.php');
  $selectedData = $_SESSION["stockManagement_SelectedProductList"];

  $quary = "SELECT * FROM `stock` INNER join `product`where stock.product_id =  product.id and product.store_id = " . $store_id;
  $resultDataList = mysqli_query($conn, $quary);

  $quary = "SELECT * FROM `category` where sub_cat_id = 0 and store_id = " . $store_id;
  $resultCatList = mysqli_query($conn, $quary);
} else {
  header("location: login.php");
}

?>


<!DOCTYPE html>
<html lang="en">

<head>
  <?php include_once('partials/header_common.php');  ?>

  <!-- iCheck -->
  <link href="public/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
  <!-- Datatables -->
  <link href="public/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
  <link href="public/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
  <link href="public/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
  <link href="public/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
  <link href="public/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

  <!-- bootstrap-daterangepicker -->
  <link href="public/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
  <!-- bootstrap-datetimepicker -->
  <link href="public/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">

  <Style>
    .ProductListStyle {
      background-color: white;
      font-weight: bold;
      border-radius: 20px;
      border: 2px solid #73AD21;
      width: auto;
      height: auto;
      padding: 5px 30px 5px 30px;
      font-size: 18px;
    }

    hr {
      display: block;
      height: 1px;
      background: transparent;
      width: 100%;
      border: none;
      border-top: solid 1px #aaa;
    }
  </Style>


</head>

<body class="nav-md">
  <div class="container body">
    <div class="main_container">
      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">

          <!-- menu profile quick info -->
          <?php include_once('partials/left_user_prodile.php');  ?>
          <!-- /menu profile quick info -->

          <br />

          <!-- sidebar menu -->
          <?php include_once('partials/left_menu.php');  ?>
          <!-- /sidebar menu -->

          <!-- /menu footer buttons -->
          <?php include_once('partials/left_buttom_menu.php');  ?>
          <!-- /menu footer buttons -->

        </div>
      </div>

      <!-- top navigation -->
      <?php include_once('partials/right_menu.php');  ?>
      <!-- /top navigation -->

      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>Sells Product<small></small></h3>
            </div>

            <!-- 
            <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  <form action="index.php" method="post">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <input type="button" onclick="submitalsubmital()" value="sybmit" class="btn btn-default">
                    <span class="input-group-btn">
                      <button class="btn btn-default" onclick="submitalsubmital()" type="button">Go!</button>
                    </span>
                  </form>

                </div>
              </div>
            </div> -->

            <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  <input type="text" id="searchItemForAdd" onchange="submitalsubmital()" class="form-control" placeholder="Use Scanner">
                  <span class="input-group-btn">
                    <button class="btn btn-default" type="button">Go!</button>
                  </span>
                </div>
              </div>
            </div>
          </div>

          <div class="clearfix"></div>


          <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">

              <div class="x_panel">
                <div class="x_title">
                  <h2>Product List</h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Product Name</th>
                        <th>Action</th>
                        <th>Stock</th>
                        <th>Qty In Box</th>
                        <th>Category</th>
                        <th>Sub Category</th>
                        <th>Sell Price</th>
                        <th>Generic</th>
                      </tr>
                    </thead>

                    <tbody>

                      <?php
                      $i = 1;
                      while ($row = mysqli_fetch_array($resultDataList)) {

                        $quary = "SELECT * FROM `category` where id = " . $row['product_type'] . " and store_id = " . $store_id;
                        $resultCatName = mysqli_query($conn, $quary);
                        $catname = "Unknown";
                        while ($ro = mysqli_fetch_array($resultCatName)) {
                          $catname = $ro['cat_name'];
                        }
                      ?>

                        <tr>
                          <td><?php echo $i++ ?></td>
                          <td><?php echo $row['product_name'] ?></td>
                          <td>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#Modal_addToCard" onclick="changeProductInfo('<?php echo $row['id']; ?>', '<?php echo $row['product_name']; ?>','<?php echo $row['sell_price']; ?>', '<?php echo $row['qty_in_box'] ?>' )">Add To List</button>
                          </td>
                          <td><?php echo $row['current_stock'] ?></td>
                          <td><?php echo $row['qty_in_box'] ?></td>
                          <td><?php echo $catname; ?></td>
                          <td><?php echo $row['product_subtype'] ?></td>
                          <td><?php echo $row['sell_price'] ?></td>
                          <td><?php echo $row['generic'] ?></td>
                        </tr>
                      <?php   }  ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <!-- End Table -->

            <!-- Card Start  -->

            <div class="col-md-6 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Invoice</h2>

                  <div class="clearfix"></div>
                </div>


                <div class="row">
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                      <div class="x_title">
                        <h2>Selected Product</small></h2>
                        <div class="clearfix"></div>
                      </div>
                      <div class="x_content">
                        <table class="table table-hover">
                          <thead>
                            <tr>
                              <th>#</th>
                              <th>Name</th>
                              <th>Qty</th>
                              <th>Qty As</th>
                              <th>Price</th>
                              <th>Sell Price</th>
                              <!-- <th>Discount (%)</th> -->
                            </tr>
                          </thead>
                          <tbody>

                            <?php
                            $subTotal = 0;
                            $discountTotal = 0;
                            $Total = 0;

                            for ($x = 0; $x < count($selectedData); $x++) {
                              $rowSellPrice = ($selectedData[$x][6] - ($selectedData[$x][4] * .01 * $selectedData[$x][6]));
                              $rowTotalQty = 1;

                              if ($selectedData[$x][2] == 'Box') {
                                $rowTotalQty = $selectedData[$x][1] * $selectedData[$x][7];
                              } else {
                                $rowTotalQty = $selectedData[$x][1];
                              }

                              $subTotal += $selectedData[$x][6] * $rowTotalQty;
                              $Total += $rowSellPrice * $rowTotalQty;
                              $discountTotal = $subTotal - $Total;

                            ?>
                              <tr>
                                <th scope="row"><?php echo $x + 1; ?></th>
                                <td><?php echo $selectedData[$x][0]; ?></td>
                                <td><?php echo $selectedData[$x][1]; ?></td>
                                <td><?php echo $selectedData[$x][2]; ?></td>
                                <td><?php echo $selectedData[$x][6] * $rowTotalQty; ?></td>
                                <td><?php echo $rowSellPrice * $rowTotalQty; ?></td>
                                <!-- <td><?php //echo $selectedData[$x][4]; 
                                          ?></td> -->
                                <td>
                                  <form id="demo-form2" method="POST" action="<?php echo $_SERVER["PHP_SELF"]; ?>" enctype="multipart/form-data" role="form" data-parsley-validate class="form-horizontal form-label-left">
                                    <input type="hidden" id="productId" name="productId" value="<?php echo $selectedData[$x][3]; ?>">
                                    <button type="Submit" name="removeToCard" class="btn btn-danger">-</button>

                                  </form>
                                </td>
                              </tr>
                            <?php } ?>
                          </tbody>
                        </table>

                      </div>
                    </div>
                  </div>
                </div>


                <div class="x_content">
                  <br />
                  <form id="form_add_sell_invoice" method="POST" action="<?php echo $_SERVER["PHP_SELF"]; ?>" enctype="multipart/form-data" role="form" class="form-horizontal form-label-left">
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3">Invoice No: *</label>
                      <div class="col-md-9 col-sm-9 col-xs-9">
                        <input type="text" id="invoice_no" required="required" name="invoice_no" value="<?php echo $time; ?>" class="form-control col-md-7 col-xs-12" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3">Customer Name: * </label>
                      <div class="col-md-9 col-sm-9 col-xs-9">
                        <input type="text" id="customer_name" required="required" name="customer_name" class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3">Tran Type</label>
                      <div class="col-md-9 col-sm-9 col-xs-9">
                        <select id="invoice_type" name="invoice_type" class="form-control" required>
                          <option value="Sell To Customer">Sell To Customer</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3">SubTotal</label>
                      <div class="col-md-9 col-sm-9 col-xs-9">
                        <input type="number" min="0" id="inv_subtotal" required="required" name="inv_subtotal" value="<?php echo $subTotal; ?>" class="form-control col-md-7 col-xs-12" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3">Discount</label>
                      <div class="col-md-9 col-sm-9 col-xs-9">
                        <input type="number" min="0" id="inv_discount" onkeyup="changeDiscount(event)" required="required" name="inv_discount" value="<?php echo $discountTotal; ?>" class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3"> Total</label>
                      <div class="col-md-9 col-sm-9 col-xs-9">
                        <input type="number" min="0" id="inv_total" required="required" name="inv_total" value="<?php echo $Total; ?>" class="form-control col-md-7 col-xs-12" readonly>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3"> Paid</label>
                      <div class="col-md-6 col-sm-6 col-xs-6">
                        <input type="number" min="0" id="inv_paid" onkeyup="getPaid(event)" required="required" name="inv_paid" value="0" class="form-control col-md-7 col-xs-12">
                      </div>
                      <div class="col-md-3 col-sm-3 col-xs-3">
                        <input type="button" onclick="fullyPaid()" class="btn btn-success" value="Fully Paid">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3"> Due</label>
                      <div class="col-md-9 col-sm-9 col-xs-9">
                        <input type="number" min="0" id="inv_due" required="required" name="inv_due" value="<?php echo $Total; ?>" class="form-control col-md-7 col-xs-12" readonly>
                      </div>
                    </div>



                    <?php if (count($selectedData) > 0) { ?>
                      <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                          <button type="submit" name="saveInvoice" class="btn btn-success">Save</button>
                          <button type="button" data-toggle="modal" onclick="setPrinterValue()" data-target="#Modal_Test" class="btn btn-success">Save & print</button>
                        </div>
                      </div>
                    <?php } ?>

                  </form>
                </div>
              </div>
            </div>

            <!-- card End  -->
          </div>
        </div>
      </div>
      <!-- /page content -->

      <!--  mode start Modal_addToCard  -->

      <div class="modal fade bs-example-modal-sm" id="Modal_addToCard" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm">
          <div class="modal-content">

            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
              </button>
              <h4 class="modal-title" id="myModalLabel2">Product </h4>
            </div>
            <form id="demo-form2" method="POST" action="<?php echo $_SERVER["PHP_SELF"]; ?>" enctype="multipart/form-data" role="form" data-parsley-validate class="form-horizontal form-label-left">

              <div class="modal-body">
                <div class="form-group">
                  <label class="col-md-12 col-sm-12 col-xs-12" for="productName1">Product
                    Name
                  </label>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <input type="text" id="productName1" required="required" name="productName" class="form-control col-md-7 col-xs-12" readonly>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-md-12 col-sm-12 col-xs-12" for="productAmount"> Qty <span class="required">*</span>
                  </label>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <input type="number" id="productAmount" value="0" name="productAmount" class="form-control col-md-7 col-xs-12" required="required">
                  </div>
                </div>

                <div class="form-group clearfix">
                  <label class="col-md-12 col-sm-12 col-xs-12" for="product_buy_type"> Qty As <span class="required">*</span>
                  </label>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <select id="product_buy_type" name="product_buy_type" class="form-control" required>
                      <option value="Box">Box</option>
                      <option value="Pcs">Pcs</option>
                    </select> </div>
                </div>

                <div class="form-group">
                  <label class="col-md-12 col-sm-12 col-xs-12" for="productDiscount"> Discount (percent) <span class="required">*</span>
                  </label>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <input type="number" id="productDiscount" value="0" name="productDiscount" class="form-control col-md-7 col-xs-12" required="required">
                  </div>
                </div>


                <input type="hidden" id="productId1" name="productId">
                <input type="hidden" id="sellPrice" name="sellPrice">
                <input type="hidden" id="qty_in_box" name="qty_in_box">


              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" name="addToCard" class="btn btn-primary">Add</button>
              </div>
            </form>
          </div>
        </div>
      </div>

      <!-- modal end  -->


      <!--  mode start Modal_Test Printer -->

      <div class="modal fade bs-example-modal-lg" id="Modal_Test" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">

            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
              </button>
              <h4 class="modal-title" style="text-align: center" id="myModalLabel">Memo</h4>
            </div>
            <div class="modal-body">
              <div id="printing_div" class="float-right" style="width: 350px; align-content: center ;">
                <div style="width: 340px;">
                  <h1 style="text-align: center"><?php echo $store_name; ?></h1>
                  <p style="text-align: center"> Store No: 49,60. Rainkhola, Road-1, Block-A, Mirpur-2, Dhaka-1216</p>
                  <p> <b>Invoice No: </b> <?php echo $time; ?> </p>
                  <p> Date: <?php echo $date; ?> </p>
                  <p>Creator: <b> <?php echo $invoiceCreatName; ?> </b></p>
                  <h3 class="ProductListStyle" style="text-align: center">Product List</h3>
                  <hr>
                </div>

                <div style="width: 345px;">

                  <div style="width: 120px; float: left">
                    <p style="text-align: left">Name</p>
                  </div>
                  <div style="width: 100px; float: left">
                    <p style="text-align: right">Qty</p>
                  </div>
                  <div style="width: 120px; float: left">
                    <p style="text-align: right">Price</p>
                  </div>
                  <div style="width: 20px; float: left">
                  </div>
                </div>

                <div style="width: 345px;">
                  <p>----------------------------------------------------------------</p>
                </div>

                <?php
                for ($x = 0; $x < count($selectedData); $x++) {
                  $rowSellPrice = ($selectedData[$x][6] - ($selectedData[$x][4] * .01 * $selectedData[$x][6]));
                  $rowTotalQty = 1;

                  if ($selectedData[$x][2] == 'Box') {
                    $rowTotalQty = $selectedData[$x][1] * $selectedData[$x][7];
                  } else {
                    $rowTotalQty = $selectedData[$x][1];
                  }

                ?>

                  <div style="width: 345px;">

                    <div style="width: 120px; float: left">
                      <p style="text-align: left"><?php echo $selectedData[$x][0]; ?></p>
                    </div>
                    <div style="width: 100px; float: left">
                      <p style="text-align: right"><?php echo $selectedData[$x][1]; ?> (<?php echo $selectedData[$x][2]; ?>)</p>
                    </div>
                    <div style="width: 120px; float: left">
                      <p style="text-align: right"><?php echo $rowSellPrice * $rowTotalQty; ?> Tk.</p>
                    </div>

                  </div>

                <?php } ?>

                <div style="width: 345px;">

                  <p style="text-align: center; color: white">s</p>
                  <hr>
                  <!-- <p>----------------------------------------------------------------</p> -->
                </div>

                <div style="width: 345px;">

                  <div style="width: 120px; float: left">
                    <p style="text-align: left">Sub Total</p>
                  </div>
                  <div style="width: 120px; float: right">
                    <p id="p_sub_total" style="text-align: right">200 Tk.</p>
                  </div>

                </div>

                <div style="width: 345px;">

                  <div style="width: 120px; float: left">
                    <p style="text-align: left">Discount</p>
                  </div>
                  <div style="width: 120px; float: right">
                    <p id="p_discount" style="text-align: right">0 Tk.</p>
                  </div>

                </div>

                <div style="width: 345px;">

                  <div style="width: 120px; float: left">
                    <p style="text-align: left">Total</p>
                  </div>
                  <div style="width: 120px; float: right">
                    <p id="p_total" style="text-align: right">200 Tk.</p>
                  </div>

                </div>

                <div style="width: 345px;">

                  <div style="width: 120px; float: left">
                    <p style="text-align: left">Paid: </p>
                  </div>
                  <div style="width: 120px; float: right">
                    <p id="p_paid" style="text-align: right">200 Tk.</p>
                  </div>

                </div>

                <div style="width: 355px;">
                  <div style="width: 345px;float: left">
                    <p style="text-align: center; color: white">s</p>
                    <hr>
                    <p></p>
                    <p style="text-align: center"> @ <?php echo $store_name; ?> | Rename Tech. Thank You </p>

                  </div>
                </div>

              </div>

              <div style="height: 130px">
                <h6>Print Memo</h6>
              </div>

            </div>
            <div class="modal-footer">
              <p style="text-align: center"> @ <?php echo $store_name; ?> . | Rename Tech. </p>
            </div>

          </div>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="printMemo()">Print</button>

        </div>
      </div>

      <!-- modal end Test -->



      <!-- footer content -->
      <?php include_once('partials/footer.php');  ?>
      <!-- /footer content -->
    </div>
  </div>

  <!-- jQuery -->
  <script src="public/vendors/jquery/dist/jquery.min.js"></script>
  <!-- Bootstrap -->
  <script src="public/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- FastClick -->
  <script src="public/vendors/fastclick/lib/fastclick.js"></script>
  <!-- NProgress -->
  <script src="public/vendors/nprogress/nprogress.js"></script>
  <!-- iCheck -->
  <script src="public/vendors/iCheck/icheck.min.js"></script>
  <!-- Datatables -->
  <script src="public/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="public/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
  <script src="public/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
  <script src="public/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
  <script src="public/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
  <script src="public/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
  <script src="public/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
  <script src="public/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
  <script src="public/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
  <script src="public/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
  <script src="public/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
  <script src="public/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
  <script src="public/vendors/jszip/dist/jszip.min.js"></script>
  <script src="public/vendors/pdfmake/build/pdfmake.min.js"></script>
  <script src="public/vendors/pdfmake/build/vfs_fonts.js"></script>


  <!-- Custom Theme Scripts -->
  <script src="public/build/js/custom.min.js"></script>


  <script>
    function changeProductInfo(id, name, sellPrice, qty_in_box) {
      // alert(sellPrice);
      document.getElementById("productName1").value = name;
      document.getElementById("productId1").value = id;
      document.getElementById("productAmount").value = 1;
      document.getElementById("sellPrice").value = sellPrice;
      document.getElementById("qty_in_box").value = qty_in_box;

    }

    function changeDiscount(event) {
      let subtotal = document.getElementById("inv_subtotal").value;
      let discount = document.getElementById("inv_discount").value;

      if (((event.key >= 0 && event.key <= 9) || event.key == '.' || event.key == 'Backspace') && (subtotal - discount) > 0) {
        document.getElementById("inv_total").value = subtotal - discount;
      } else {
        document.getElementById("inv_total").value = subtotal;
        document.getElementById("inv_discount").value = 0;
      }

      calculateDue();

    }

    function getPaid(event) {
      if (!((event.key >= 0 && event.key <= 9) || event.key == '.' || event.key == 'Backspace')) {
        document.getElementById("inv_due").value = document.getElementById("inv_total").value;
        document.getElementById("inv_paid").value = 0;
      }
      calculateDue();
    }

    function calculateDue() {
      let paid = document.getElementById("inv_paid").value;
      let total = document.getElementById("inv_total").value;


      if ((total - paid) > 0) {
        document.getElementById("inv_due").value = total - paid;
      } else {
        document.getElementById("inv_due").value = total;
      }
    }

    function fullyPaid() {
      document.getElementById("inv_due").value = "0";
      document.getElementById("inv_paid").value = document.getElementById("inv_total").value;
    }

    function alert1() {
      let data = array()
    }

    function submitalsubmital() {
      $.ajax({
        type: "POST",
        url: "controller/scanForSale.php",
        data: 'id=' + document.getElementById("searchItemForAdd").value,
        success: function(response) {
          // console.log(response);
          var json = $.parseJSON(response);
          if (json.status == 'error')
            console.log(json.message);
          else if (json.status == 'success') {
            changeProductInfo(json.objectJson['product_id'], json.objectJson['product_name'], json.objectJson['sell_price'], json.objectJson['qty_in_box']);
            // console.log(json.objectJson);
            $("#Modal_addToCard").modal('show');
          }
          document.getElementById("searchItemForAdd").value = "";
        }
      });
    }

    function printMemo() {
      console.log("cala");
      // let generator = window.open(",'name,");
      let generator = window.open('', '_blank', 'width=410');
      let layetext = document.getElementById('printing_div');
      generator.document.write(layetext.innerHTML.replace("Print Me"));
      generator.document.close();
      generator.print();
      generator.close();
      // document.getElementById("form_add_sell_invoice").submit();
      // document.form_add_sell_invoice.submit();

      // console.log("cala2");
      // document.forms["form_add_sell_invoice"].submit();
    }

    function setPrinterValue() {
      document.getElementById('p_paid').innerHTML = document.getElementById('inv_paid').value + " tk";
      document.getElementById('p_total').innerText = document.getElementById('inv_total').value + " tk";
      document.getElementById('p_sub_total').innerHTML = document.getElementById('inv_subtotal').value + " tk";
      // document.getElementById('p_discount').innerHTML = document.getElementById('inv_discount').value + " tk";

    }
  </script>

  <?php include_once('partials/notification.php');  ?>

</body>

</html>