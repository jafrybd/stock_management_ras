-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 11, 2021 at 07:30 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `stock managemet system_ras`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `cat_name` varchar(155) NOT NULL,
  `cat_description` text NOT NULL,
  `sub_cat_id` int(11) NOT NULL DEFAULT 0,
  `store_id` int(11) NOT NULL,
  `creation_date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `cat_name`, `cat_description`, `sub_cat_id`, `store_id`, `creation_date`) VALUES
(1, 'Medicine', 'ssf', 0, 1, '2019-11-18 18:34:08'),
(3, 'General', 'ssdf', 0, 1, '2019-11-18 19:52:27'),
(4, 'xsag', 'as', 0, 1, '2019-11-18 21:41:21'),
(5, 'Tablet', 'cvb', 1, 1, '2019-11-18 21:42:47'),
(6, 'Milk', 'as', 3, 1, '2019-11-18 21:50:32'),
(7, 'As test', 'dssdf', 0, 1, '2019-11-19 21:46:29'),
(8, 'As 1', 's', 7, 1, '2019-11-19 21:46:52'),
(10, 'a 3', 'ca', 7, 1, '2019-11-19 21:47:04'),
(11, 'Syp', 'xs', 1, 1, '2019-11-20 13:38:34'),
(12, 'sdadads', 'sa', 0, 1, '2019-11-26 00:02:15'),
(13, 'dsad', 'sda', 0, 1, '2019-12-23 20:07:47');

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `id` int(11) NOT NULL,
  `company_name` varchar(50) NOT NULL DEFAULT 'Not Defined',
  `company_description` text DEFAULT NULL,
  `store_id` int(11) NOT NULL,
  `visibility` tinyint(1) NOT NULL DEFAULT 1,
  `creation_date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`id`, `company_name`, `company_description`, `store_id`, `visibility`, `creation_date`) VALUES
(3, 'Aristo pharma', 'aristo Pharma is a', 1, 1, '2019-11-17 20:27:41'),
(7, 'Ziska Pharma', '11fdfsjk', 1, 0, '2019-11-18 13:45:43'),
(8, 'Danu', 'Bd company', 1, 1, '2019-11-25 22:59:48');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `contact` varchar(20) NOT NULL,
  `address` text NOT NULL,
  `description` text NOT NULL,
  `amount` float NOT NULL DEFAULT 0,
  `store_id` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `dues`
--

CREATE TABLE `dues` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(255) NOT NULL DEFAULT 'unknown',
  `invoice_no` int(11) NOT NULL DEFAULT 0,
  `tran_type` varchar(50) NOT NULL DEFAULT 'not defiend',
  `amount` int(11) NOT NULL DEFAULT 0,
  `creation_time` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `dues`
--

INSERT INTO `dues` (`id`, `customer_name`, `invoice_no`, `tran_type`, `amount`, `creation_time`) VALUES
(1, 'Sheiblu', 14, 'Sell To Customer', 3300, '2020-01-05 20:19:00'),
(2, 'Sheiblu', 15, 'Sell To Customer', 20, '2020-01-05 20:20:07');

-- --------------------------------------------------------

--
-- Table structure for table `generic`
--

CREATE TABLE `generic` (
  `id` int(11) NOT NULL,
  `generic_name` varchar(155) NOT NULL DEFAULT 'Not Defined',
  `generic_description` text NOT NULL,
  `store_id` int(11) NOT NULL,
  `creation_date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `generic`
--

INSERT INTO `generic` (`id`, `generic_name`, `generic_description`, `store_id`, `creation_date`) VALUES
(2, 'New Two', 'Generic', 1, '2019-11-21 16:57:24'),
(4, 'Some Three', 'No entry', 1, '2019-11-25 22:29:09');

-- --------------------------------------------------------

--
-- Table structure for table `invoice_tran`
--

CREATE TABLE `invoice_tran` (
  `id` int(11) NOT NULL,
  `inv_no` varchar(155) NOT NULL DEFAULT 'Not Defined',
  `tran_type` varchar(25) NOT NULL,
  `sub_total` double NOT NULL DEFAULT 0,
  `total` double NOT NULL DEFAULT 0,
  `due` double NOT NULL DEFAULT 0,
  `paid` double NOT NULL DEFAULT 0,
  `discount` double NOT NULL DEFAULT 0,
  `customer_name` varchar(255) NOT NULL DEFAULT 'Unknown Name',
  `inv_creator_id` int(11) NOT NULL DEFAULT 0,
  `store_id` int(6) NOT NULL DEFAULT 0,
  `creating_date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `invoice_tran`
--

INSERT INTO `invoice_tran` (`id`, `inv_no`, `tran_type`, `sub_total`, `total`, `due`, `paid`, `discount`, `customer_name`, `inv_creator_id`, `store_id`, `creating_date`) VALUES
(1, '343', 'Receive From Supplier', 0, 0, 0, 0, 0, 'Alim', 0, 1, '2019-12-24 14:54:35'),
(2, '3232', 'Receive From Supplier', 0, 0, 0, 0, 0, 'Alim', 0, 1, '2019-12-23 14:55:09'),
(3, '1577091557', 'Sell To Customer', 3600, 3600, 0, 3600, 0, 'hu', 0, 1, '2019-12-23 15:00:43'),
(4, '987', 'Return to Supplier', 0, 0, 0, 0, 0, 'rfg', 0, 1, '2019-12-23 15:01:35'),
(5, '7999', 'Customer Return', 0, 0, 0, 0, 0, 'gjkhb', 0, 1, '2019-12-23 15:01:59'),
(6, '1577132874', 'Sell To Customer', 3600, 3600, 0, 3600, 0, 'No', 0, 1, '2019-12-24 02:28:39'),
(7, '', '', 0, 0, 0, 0, 0, 'Un Known', 0, 1, '2020-01-01 14:02:43'),
(8, '1578030836', 'Sell To Customer', 208, 196, 0, 196, 12, 'Sheiblu', 0, 1, '2020-01-03 11:58:15'),
(9, '1578227470', 'Sell To Customer', 3600, 3600, 3600, 0, 0, 'Un Known', 0, 1, '2020-01-05 18:31:54'),
(10, '1578227680', 'Sell To Customer', 3600, 3510, 0, 3510, 90, 'Sheiblu', 0, 1, '2020-01-05 18:35:35'),
(11, '1578228107', 'Sell To Customer', 44, 44, 44, 0, 0, 'Sheiblu', 0, 1, '2020-01-05 18:42:29'),
(12, '1578232040', 'Sell To Customer', 3600, 3600, 3600, 0, 0, 'Amin', 1, 1, '2020-01-05 19:47:29'),
(13, '3422', 'Receive From Supplier', 0, 0, 0, 0, 0, 'ewe', 1, 1, '2020-01-05 19:49:39'),
(14, '1578233924', 'Sell To Customer', 3600, 3600, 3300, 300, 0, 'Sheiblu', 1, 1, '2020-01-05 20:18:59'),
(15, '1578233992', 'Sell To Customer', 28, 28, 20, 8, 0, 'Sheiblu', 1, 1, '2020-01-05 20:20:06');

-- --------------------------------------------------------

--
-- Table structure for table `invoice_tran_item`
--

CREATE TABLE `invoice_tran_item` (
  `id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL DEFAULT 0,
  `tran_type` varchar(30) NOT NULL DEFAULT 'not defiend',
  `product_id` int(11) NOT NULL DEFAULT 0,
  `qty` int(11) NOT NULL DEFAULT 0,
  `price_per_rate` double NOT NULL DEFAULT 0,
  `discount` double NOT NULL DEFAULT 0,
  `store_id` int(11) NOT NULL DEFAULT 0,
  `creating_date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `invoice_tran_item`
--

INSERT INTO `invoice_tran_item` (`id`, `invoice_id`, `tran_type`, `product_id`, `qty`, `price_per_rate`, `discount`, `store_id`, `creating_date`) VALUES
(1, 1, 'Receive From Supplier', 1, 80, 0, 0, 1, '2019-12-23 14:54:35'),
(4, 3, 'Sell To Customer', 2, 20, 180, 0, 1, '2019-12-23 15:00:43'),
(5, 4, 'Return to Supplier', 3, 2, 0, 0, 2, '2019-12-23 15:01:35'),
(6, 5, 'Customer Return', 4, 1, 0, 0, 2, '2019-12-23 15:01:59'),
(7, 0, 'direct increase', 6, 1, 0, 0, 1, '2019-12-23 15:02:52'),
(8, 0, 'direct decrease', 1, 5, 0, 0, 1, '2019-12-23 15:03:02'),
(9, 6, 'Sell To Customer', 1, 20, 180, 0, 1, '2019-12-24 02:28:39'),
(10, 0, 'direct increase', 1, 22, 0, 0, 1, '2019-12-24 02:49:59'),
(11, 0, 'direct increase', 3, 34, 0, 0, 1, '2019-12-24 02:52:46'),
(12, 7, '', 1, 20, 180, 0, 1, '2020-01-01 14:02:44'),
(13, 8, 'Sell To Customer', 1, 1, 180, 2, 1, '2020-01-03 11:58:15'),
(14, 8, 'Sell To Customer', 2, 20, 1.4, 4, 1, '2020-01-03 11:58:15'),
(15, 9, 'Sell To Customer', 1, 20, 180, 0, 1, '2020-01-05 18:31:54'),
(16, 9, 'Sell To Customer', 3, 3, 0, 0, 1, '2020-01-05 18:31:54'),
(17, 10, 'Sell To Customer', 1, 20, 180, 3, 1, '2020-01-05 18:35:35'),
(18, 11, 'Sell To Customer', 4, 11, 4, 0, 1, '2020-01-05 18:42:29'),
(19, 12, 'Sell To Customer', 1, 20, 180, 0, 1, '2020-01-05 19:47:29'),
(20, 13, 'Receive From Supplier', 1, 20, 0, 0, 1, '2020-01-05 19:49:39'),
(21, 14, 'Sell To Customer', 1, 20, 180, 0, 1, '2020-01-05 20:19:00'),
(22, 15, 'Sell To Customer', 2, 20, 1.4, 0, 1, '2020-01-05 20:20:07');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `status` int(2) NOT NULL DEFAULT 1,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `product_category`
--

CREATE TABLE `product_category` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT 1,
  `status` int(2) NOT NULL DEFAULT 1,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_category`
--

INSERT INTO `product_category` (`id`, `title`, `store_id`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'sabon c', 1, 1, 0, '2021-06-11 22:42:37', '2021-06-11 22:42:37'),
(2, 'mehereb', 1, 1, 0, '2021-06-11 22:43:02', '2021-06-11 22:43:02');

-- --------------------------------------------------------

--
-- Table structure for table `product_group`
--

CREATE TABLE `product_group` (
  `id` int(11) NOT NULL,
  `group_name` varchar(255) NOT NULL,
  `status` int(2) NOT NULL DEFAULT 1,
  `store_id` int(11) NOT NULL DEFAULT 1,
  `created_by` datetime NOT NULL DEFAULT current_timestamp(),
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_group`
--

INSERT INTO `product_group` (`id`, `group_name`, `status`, `store_id`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'sdfs 1', 1, 1, '2021-06-11 23:22:47', '2021-06-11 23:22:47', '2021-06-11 23:22:47');

-- --------------------------------------------------------

--
-- Table structure for table `product_head`
--

CREATE TABLE `product_head` (
  `id` int(11) NOT NULL,
  `title` varchar(155) NOT NULL DEFAULT 'Not Defined',
  `store_id` int(11) NOT NULL,
  `status` int(2) NOT NULL DEFAULT 1,
  `creation_date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_head`
--

INSERT INTO `product_head` (`id`, `title`, `store_id`, `status`, `creation_date`) VALUES
(1, 'Rods\'s', 1, 1, '2021-06-10 22:30:40'),
(2, 'Yellow', 1, 0, '2021-06-10 22:45:48'),
(3, 'rods', 1, 1, '2021-06-10 23:03:54');

-- --------------------------------------------------------

--
-- Table structure for table `product_size`
--

CREATE TABLE `product_size` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT 1,
  `status` int(2) NOT NULL DEFAULT 1,
  `created_by` datetime NOT NULL DEFAULT current_timestamp(),
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_size`
--

INSERT INTO `product_size` (`id`, `title`, `store_id`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(1, '3 * 2 * 3', 1, 1, '2021-06-11 23:27:52', '2021-06-11 23:27:52', '2021-06-11 23:27:52');

-- --------------------------------------------------------

--
-- Table structure for table `product_type`
--

CREATE TABLE `product_type` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `status` int(2) NOT NULL DEFAULT 1,
  `store_id` int(11) NOT NULL,
  `created_by` datetime NOT NULL DEFAULT current_timestamp(),
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_type`
--

INSERT INTO `product_type` (`id`, `title`, `status`, `store_id`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'PR', 1, 1, '2021-06-11 23:12:50', '2021-06-11 23:12:50', '2021-06-11 23:12:50'),
(2, 'QR', 1, 1, '2021-06-11 23:13:33', '2021-06-11 23:13:33', '2021-06-11 23:13:33');

-- --------------------------------------------------------

--
-- Table structure for table `stock`
--

CREATE TABLE `stock` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `current_stock` int(11) NOT NULL DEFAULT 0,
  `product_name` varchar(155) NOT NULL,
  `reorder_level` int(11) NOT NULL DEFAULT 0,
  `buing_price` float NOT NULL DEFAULT 0,
  `sell_price` float NOT NULL DEFAULT 0,
  `average_price` float NOT NULL DEFAULT 0,
  `last_tran_date` datetime NOT NULL DEFAULT current_timestamp(),
  `store_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `stock`
--

INSERT INTO `stock` (`id`, `product_id`, `current_stock`, `product_name`, `reorder_level`, `buing_price`, `sell_price`, `average_price`, `last_tran_date`, `store_id`) VALUES
(1, 1, -41, 'Napa', 5, 0, 180, 0, '2020-01-05 20:18:59', 1),
(2, 2, 78, 'Milk Bita', 0, 1, 1.4, 0, '2020-01-05 20:20:07', 1),
(3, 3, 34, 'Zero Call', 0, 0, 0, 0, '2020-01-05 18:31:54', 1),
(4, 0, 0, 'Milk Bita', 0, 0, 0, 0, '2019-12-08 12:18:51', 1),
(5, 4, -14, 'New Test Product', 0, 100, 4, 0, '2020-01-05 18:42:29', 1),
(6, 5, 0, 'sdfghj', 0, 1, 1, 0, '2019-12-23 14:35:25', 1);

-- --------------------------------------------------------

--
-- Table structure for table `store`
--

CREATE TABLE `store` (
  `id` int(11) NOT NULL,
  `store_name` varchar(155) NOT NULL DEFAULT 'Unknown',
  `store_address` text NOT NULL,
  `phone` varchar(20) NOT NULL,
  `owner_name` varchar(155) NOT NULL DEFAULT 'Unknown',
  `account_type` varchar(10) NOT NULL DEFAULT 'free',
  `registration_date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `store`
--

INSERT INTO `store` (`id`, `store_name`, `store_address`, `phone`, `owner_name`, `account_type`, `registration_date`) VALUES
(1, 'Araf Pharmacy', 'Mirpur 2, zoo road.', '01671794064', 'Shihab Ahamed', 'free', '2019-11-18 12:05:09'),
(2, 'Test Pharmacy', 'Palabi, Dhaka', '01712102558', 'Text Mia', 'free', '2019-12-23 21:20:01');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `company_name` varchar(100) NOT NULL,
  `company_description` text NOT NULL,
  `contact` varchar(20) NOT NULL,
  `address` text NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT 0,
  `status` int(2) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `user_id` varchar(15) NOT NULL DEFAULT 'us-',
  `user_name` varchar(155) NOT NULL DEFAULT 'Unknown',
  `phone_no` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL DEFAULT '999999',
  `user_type` varchar(10) NOT NULL DEFAULT 'Super',
  `store_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `user_id`, `user_name`, `phone_no`, `email`, `password`, `user_type`, `store_id`) VALUES
(1, 'us-01', 'Shihab Ahamed', '01671794064', 'isheiblu@gmail.com', '$2y$10$Qft0OkDdmgBToA3/kV3jne7SF9JF4DZkJZGFivKENW4z74JstCCV2', 'Super', 1),
(2, 'us-02', 'Text Mia', '01712102558', 'testmia@gmail.com', '$2y$10$Qft0OkDdmgBToA3/kV3jne7SF9JF4DZkJZGFivKENW4z74JstCCV2', 'Super', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dues`
--
ALTER TABLE `dues`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `generic`
--
ALTER TABLE `generic`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_tran`
--
ALTER TABLE `invoice_tran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_tran_item`
--
ALTER TABLE `invoice_tran_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_category`
--
ALTER TABLE `product_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_group`
--
ALTER TABLE `product_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_head`
--
ALTER TABLE `product_head`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_size`
--
ALTER TABLE `product_size`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_type`
--
ALTER TABLE `product_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stock`
--
ALTER TABLE `stock`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `store`
--
ALTER TABLE `store`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `dues`
--
ALTER TABLE `dues`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `generic`
--
ALTER TABLE `generic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `invoice_tran`
--
ALTER TABLE `invoice_tran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `invoice_tran_item`
--
ALTER TABLE `invoice_tran_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_category`
--
ALTER TABLE `product_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `product_group`
--
ALTER TABLE `product_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `product_head`
--
ALTER TABLE `product_head`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `product_size`
--
ALTER TABLE `product_size`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `product_type`
--
ALTER TABLE `product_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `stock`
--
ALTER TABLE `stock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `store`
--
ALTER TABLE `store`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
