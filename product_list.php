<?php

session_start();

if (isset($_SESSION["showAllUnvesibleProduct"])) $showAll = true;
else $showAll = false;

if (isset($_SESSION['store_management_user_id'])) {
  $store_id = $_SESSION['store_management_store_id'];
  include_once('controller/product.php');

  if ($showAll) {
    $quary = "SELECT * FROM `product` WHERE store_id = " . $store_id;
  } else {
    $quary = "SELECT * FROM `product` WHERE visibility = 1 and store_id = " . $store_id;
  }
  $resultDataList = mysqli_query($conn, $quary);

  $quary = "SELECT * FROM `category` where sub_cat_id = 0 and store_id = " . $store_id;
  $resultCatList = mysqli_query($conn, $quary);

  $quary = "SELECT * FROM `generic` where store_id = " . $store_id;
  $resultGenericList = mysqli_query($conn, $quary);

  $quary = "SELECT * FROM `company` where store_id = " . $store_id;
  $resultCompanyList = mysqli_query($conn, $quary);
  
} else {
  header("location: login.php");
}

?>



<!DOCTYPE html>
<html lang="en">

<head>
  <?php include_once('partials/header_common.php');  ?>

  <!-- iCheck -->
  <link href="public/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
  <!-- Datatables -->
  <link href="public/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
  <link href="public/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
  <link href="public/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
  <link href="public/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
  <link href="public/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

  <!-- bootstrap-daterangepicker -->
  <link href="public/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
  <!-- bootstrap-datetimepicker -->
  <link href="public/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
  <script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>
  <script>
    function getSubCat(val) {
      // alert("i was call : "+val);
      $.ajax({
        type: "POST",
        url: "controller/product_subCat_dropdown.php",
        data: 'countryid=' + val,
        success: function(data) {
          $("#product_sub_category").html(data);
        }
      });
    }
  </script>

</head>

<body class="nav-md">
  <div class="container body">
    <div class="main_container">
      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">

          <!-- menu profile quick info -->
          <?php include_once('partials/left_user_prodile.php');  ?>
          <!-- /menu profile quick info -->

          <br />

          <!-- sidebar menu -->
          <?php include_once('partials/left_menu.php');  ?>
          <!-- /sidebar menu -->

          <!-- /menu footer buttons -->
          <?php include_once('partials/left_buttom_menu.php');  ?>
          <!-- /menu footer buttons -->

        </div>
      </div>

      <!-- top navigation -->
      <?php include_once('partials/right_menu.php');  ?>
      <!-- /top navigation -->

      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>Product List<small></small></h3>
            </div>

            <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="Search for...">
                  <span class="input-group-btn">
                    <button class="btn btn-default" type="button">Go!</button>
                  </span>
                </div>
              </div>
            </div>
          </div>

          <div class="clearfix"></div>

          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <h3><?php echo $message;  ?></h3>
              <div class="x_panel">
                <div class="x_content">

                  <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Product Name</th>
                        <th>Category</th>
                        <th>Sub Category</th>
                        <th>Generic</th>
                        <th>Qty In Box</th>
                        <th>Company</th>
                        <th>Description</th>
                        <?php if ($showAll) { ?>
                          <th> visibility</th>
                        <?php   } ?>
                        <th>Creation Date</th>
                        <th>Action</th>
                      </tr>
                    </thead>

                    <tbody>
                      <?php
                      $i = 1;
                      while ($row = mysqli_fetch_array($resultDataList)) {

                        $quary = "SELECT * FROM `category` where id = '" . $row['product_type'] . "' and store_id = " . $store_id;
                        $resultCatName = mysqli_query($conn, $quary);
                        $catName = "Unknown";
                        // $catName = $quary;
                        while ($ro = mysqli_fetch_array($resultCatName)) {
                          $catName = $ro['cat_name'];
                        }

                        $quary = "SELECT * FROM `company` where id = " . $row['company_id'] . " and store_id = " . $store_id;
                        $companyName = "Unknown";
                        $resultCompanyName = mysqli_query($conn, $quary);
                        while ($ro = mysqli_fetch_array($resultCompanyName)) {
                          $companyName = $ro['company_name'];
                        }

                      ?>

                        <tr>
                          <td><?php echo $i++ ?></td>
                          <td><?php echo $row['product_name'] ?></td>
                          <td><?php echo $catName; ?></td>
                          <td><?php echo $row['product_subtype'] ?></td>
                          <td><?php echo $row['generic'] ?></td>
                          <td><?php echo $row['qty_in_box'] ?></td>
                          <td><?php echo $companyName; ?></td>
                          <td><?php echo $row['description'] ?></td>
                          <?php if ($showAll) { ?>
                            <td><?php if ($row['visibility']) echo "True";
                                else echo "False" ?>
                            </td>
                          <?php   } ?>

                          <td><?php echo $row['creation_date'] ?></td>
                          <td>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#Modal_edit" onclick="changeEditProductInfo('<?php echo $row['id'] ?>', '<?php echo $row['product_name']; ?>', '<?php echo $row['qty_in_box'] ?>', '<?php echo $row['product_type'] ?>', '<?php echo $row['product_subtype'] ?>', '<?php echo $row['generic'] ?>', '<?php echo $row['description'] ?>', '<?php echo $row['visibility'] ?>', '<?php echo $row['company_id']; ?>')">Edit</button>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#Modal_delete" onclick="changeDeleteProductInfo('<?php echo $row['id'] ?>', '<?php echo $row['product_name'] ?>')">Delete</button>
                          </td>
                        </tr>
                      <?php   }  ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /page content -->

      <!--  mode start Delete -->

      <div class="modal fade bs-example-modal-sm" id="Modal_delete" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm">
          <div class="modal-content">

            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
              </button>
              <h4 class="modal-title" id="myModalLabel2">Warning</h4>
            </div>
            <div class="modal-body">
              <h4 id="deleteProductMessage">Will </h4>
            </div>
            <form id="demo-form2" method="POST" action="<?php echo $_SERVER["PHP_SELF"]; ?>" enctype="multipart/form-data" role="form" data-parsley-validate class="form-horizontal form-label-left">
              <div class="modal-footer">
                <input id="deleteProductId" name="deleteProductId" type="hidden" value="0">
                <input type="submit" name="productDelete" class="btn btn-danger" value="Yes">
                <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                <!-- <button type="submit" class="btn btn-success">Submit</button> -->
              </div>
            </form>
          </div>
        </div>
      </div>

      <!-- modal end  -->

      <!--  mode start Edit  -->

      <div class="modal fade bs-example-modal-sm" id="Modal_edit" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm">
          <div class="modal-content">

            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
              </button>
              <h4 class="modal-title" id="myModalLabel2">Edit Product</h4>
            </div>
            <form id="demo-form2" method="POST" action="<?php echo $_SERVER["PHP_SELF"]; ?>" enctype="multipart/form-data" role="form" data-parsley-validate class="form-horizontal form-label-left">

              <div class="modal-body">
                <div class="form-group">
                  <label class="col-md-12 col-sm-12 col-xs-12" for="first-name">Product
                    Name <span class="required">*</span>
                  </label>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <input type="text" id="productName" required="required" readonly name="product_name" class="form-control col-md-7 col-xs-12">
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-md-12 col-sm-12 col-xs-12" for="product_category"> Product Category
                    <span class="required">*</span>
                  </label>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <select id="product_category" name="product_category" class="form-control" onChange="getSubCat(this.value);" required>
                      <option value="-10"> Select a Category</option>

                      <?php while ($row = mysqli_fetch_array($resultCatList)) {   ?>
                        <option value="<?php echo $row['id']; ?>"> <?php echo $row["cat_name"]; ?></option>
                      <?php   }  ?>

                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-md-12 col-sm-12 col-xs-12" for="product_sub_category"> Product Sub Category <span class="required">*</span>
                  </label>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <select id="product_sub_category" name="product_sub_category" class="form-control" required>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-md-12 col-sm-12 col-xs-12" for="product_generic"> Generic <span class="required">*</span>
                  </label>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <select id="product_generic" name="product_generic" class="form-control" required>
                      <option value="press">Select Generic</option>
                      <?php while ($row = mysqli_fetch_array($resultGenericList)) {   ?>
                        <option value="<?php echo $row['generic_name']; ?>"> <?php echo $row["generic_name"]; ?></option>
                      <?php   }  ?>
                    </select>
                  </div>
                </div>


                <div class="form-group">
                  <label class="col-md-12 col-sm-12 col-xs-12" for="product_company"> Product Company <span class="required">*</span>
                  </label>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <select id="product_company" name="product_company" class="form-control" required>
                      <option value="press">Select Company</option>
                      <?php while ($row = mysqli_fetch_array($resultCompanyList)) {   ?>
                        <option value="<?php echo $row['id']; ?>"> <?php echo $row["company_name"]; ?></option>
                      <?php   }  ?>
                    </select>
                  </div>
                </div>


                <div class="form-group">
                  <label class="col-md-12 col-sm-12 col-xs-12" for="qty_in_box"> Product Qty in Box <span class="required">*</span>
                  </label>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <input type="number" min="1" id="qty_in_box" required="required" name="qty_in_box" class="form-control col-md-7 col-xs-12">
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-md-12 col-sm-12 col-xs-12" for="form_visibility"> Product Visibility <span class="required">*</span>
                  </label>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <input type="checkbox" name="form_visibility" checked />
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-md-12 col-sm-12 col-xs-12" for="productDescription"> Description <span class="required">*</span>
                  </label>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <textarea rows="4" id="productDescription" class="form-control col-md-7 col-xs-12" type="text" name="discription"></textarea>
                  </div>
                </div>

                <input type="hidden" id="productId" name="productId">
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" name="ProductDetailsUpdate" class="btn btn-primary">Save changes</button>
              </div>
            </form>
          </div>
        </div>
      </div>

      <!-- modal end  -->



      <!-- footer content -->
      <?php include_once('partials/footer.php');  ?>
      <!-- /footer content -->
    </div>
  </div>

  <!-- jQuery -->
  <script src="public/vendors/jquery/dist/jquery.min.js"></script>
  <!-- Bootstrap -->
  <script src="public/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- FastClick -->
  <script src="public/vendors/fastclick/lib/fastclick.js"></script>
  <!-- NProgress -->
  <script src="public/vendors/nprogress/nprogress.js"></script>
  <!-- iCheck -->
  <script src="public/vendors/iCheck/icheck.min.js"></script>
  <!-- Datatables -->
  <script src="public/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="public/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
  <script src="public/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
  <script src="public/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
  <script src="public/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
  <script src="public/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
  <script src="public/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
  <script src="public/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
  <script src="public/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
  <script src="public/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
  <script src="public/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
  <script src="public/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
  <script src="public/vendors/jszip/dist/jszip.min.js"></script>
  <script src="public/vendors/pdfmake/build/pdfmake.min.js"></script>
  <!-- Switchery -->
  <script src="public/vendors/switchery/dist/switchery.min.js"></script>
  <script src="public/vendors/pdfmake/build/vfs_fonts.js"></script>


  <!-- Custom Theme Scripts -->
  <script src="public/build/js/custom.min.js"></script>


  <script>
    function changeDeleteProductInfo(id, name) {
      document.getElementById("deleteProductMessage").innerHTML = "Will You Really Delete <span style='color:blue'>" + name + "</span> Product";
      document.getElementById("deleteProductId").value = id;
    }

    function changeEditProductInfo(id, name, qty_in_box, product_type, product_subtype, generic, description, visibility, company_id) {

      document.getElementById("productName").value = name;
      document.getElementById("product_category").value = product_type;
      document.getElementById("productDescription").value = description;
      getSubCat(document.getElementById("product_category").value);
      // sub(visibility); 
      document.getElementById("product_generic").value = generic;
      document.getElementById("product_company").value = company_id;
      document.getElementById("qty_in_box").value = qty_in_box;
      document.getElementById("productId").value = id;
      document.getElementById("form_visibility").value = 1;

    }

    function sub() {
      alert(valu);
    }
  </script>

  <?php include_once('partials/notification.php');  ?>

</body>

</html>